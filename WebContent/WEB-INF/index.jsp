<c:import url="menubar.jsp"></c:import>

<main>	
	   <c:if test="${ !empty plist }">
	   	<h4 style="margin-top:20px;">Nouveau produit :</h4>
			<c:forEach var="item" items="${ plist }">
				<section style="display:flex;background-color:whitesmoke;margin-top:10px;padding:10px;">
					<section style="flex:1;">
						<c:choose>
							<c:when test="${ !empty  item.getImages() }">
									<img id="slider" style="max-width:200px;max-height:500px;min-height:50px;" src="/millionrama/productimg/<c:out value="${ item.getImages()[0].getId() }"></c:out>_ipid=<c:out value="${ item.getImages()[0].getFileName() }"></c:out>"/>
							</c:when>
							<c:otherwise>
								<img id="slider" style="max-width:200px;max-height:500px;min-height:50px;" src="/millionrama/images/noimg.jpg"/>
							</c:otherwise>
						</c:choose>
					</section>
					<section style="flex:3.5;">
						<h3><c:out value="${ item.getName() }"></c:out></h3>
						<p style="margin-top:5px;"><c:out value="${ item.getDescription() }"></c:out></p>
						<p style="font-weight:600;margin-top:5px;"><c:out value="${ item.getPrice() }$"></c:out></p>
						<a style="color:#008AE6;text-decoration:underline;margin-top:5px;" href="p?id=<c:out value="${ item.getId() }"></c:out>">See more</a>
					</section>
				</section>
			</c:forEach>
		</c:if>
</main>

<c:import url="footer.jsp"></c:import>