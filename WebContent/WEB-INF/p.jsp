<c:import url="menubar.jsp"></c:import>

<main id="pmain">
	<section id="productglobal">
		
			<c:choose>
				<c:when test="${ !empty product.getImages() }">
					<section class="g">			
						<section id="pmainImagePrincipal">
							<img id="slider" style="max-width:500px;max-height:300px;min-height:300px;" src="/millionrama/productimg/<c:out value="${ product.getImages()[0].getId() }"></c:out>_ipid=<c:out value="${ product.getImages()[0].getFileName() }"></c:out>"/>
						</section>
						<c:forEach var="item" items="${ product.getImages() }">
							<img  style="max-width:50px;max-height:30px;min-height:30px;" class="imgprod" style="max-width:50px;" src="/millionrama/productimg/<c:out value="${ item.getId() }"></c:out>_ipid=<c:out value="${ item.getFileName() }"></c:out>"/>
						</c:forEach>
					</section>
				</c:when>
				<c:otherwise>
					<section id="pmainImagePrincipal">
						<img id="slider" style="max-width:500px;max-height:300px;min-height:300px;" src="/millionrama/images/noimg.jpg"/>
					</section>
				</c:otherwise>
			</c:choose>
		<section class="g" style="padding-left:10px;">
			<h3><c:out value="${ product.getName() }"></c:out></h3>
			<p class="ing"><c:out value="${ product.getDescription() }"></c:out></p>
			<p class="ing"><c:out value="${ product.getPrice() }"></c:out>$</p>		
			
			<c:choose>
				<c:when test="${ product.getQuantity() > 0 }">					
					<c:choose>
						<c:when test="${ alreadyIn }">
							<a style="color:white;">
								<div style="text-align:center;margin-top:10px;width:150px;background-color:#0099FF;display:flex;padding:5px;" id="submitRemovePanier">
									<p id="change" style="flex:1;padding-top:5px;padding-bottom:5px;">Remove</p>
									<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderRemovePanier"/>
								</div>
							</a>
						</c:when>
						<c:otherwise>
							<a style="color:white;">
								<div style="text-align:center;margin-top:10px;width:150px;background-color:#0099FF;display:flex;padding:5px;" id="submitAddPanier">
									<p id="change" style="flex:1;padding-top:5px;padding-bottom:5px;">Add to cart</p>
									<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderAddPanier"/>
								</div>
							</a>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<p class="oos ing">Out of stock</p>
				</c:otherwise>
			</c:choose>
		</section>
	</section>
</main>

<script src="${pageContext.request.contextPath}/script/ajax.js"></script>
<script>
	var tabimg = document.getElementsByClassName("imgprod");
	var slider = document.getElementById("slider");
	
	for(var i = 0 ; i <= tabimg.length-1 ; i++)
	{
        tabimg[i].onclick = (function(src)
		        {
		            return function() 
		            {
		               slider.src = src;
		               
		               for(var i = 0 ; i <= tabimg.length-1 ; i++)
		            	   tabimg[i].style.border = "0";
		               
		               this.style.border = "2px solid #0099FF";
		            }
		        })(tabimg[i].src);
	}
	
	
	
	//requete ajax pour ajouter au panier
	
	var submitAddPanier = null;
	var loaderAddPanier = null;
	
	if(document.getElementById("submitAddPanier") != null)
		submitAddPanier = document.getElementById("submitAddPanier");
	else
		submitAddPanier = document.getElementById("submitRemovePanier")
		
	if(document.getElementById("loaderAddPanier") != null)
		loaderAddPanier = document.getElementById("loaderAddPanier");
	else
		loaderAddPanier = document.getElementById("loaderRemovePanier")
	
	var change = document.getElementById("change");
	var xhraddpanier = null;
	
	submitAddPanier.onclick = function(e)
	{
		if(submitAddPanier.id == "submitAddPanier")
		{
			var addform = new FormData();
			addform.append("id",<%=request.getParameter("id")%>);	
			requestV2(addPanier , "test" , addform , loaderAddPanier , submitAddPanier , null , xhraddpanier );
		}
		else if(submitAddPanier.id == "submitRemovePanier")
		{
			var addform = new FormData();
			addform.append("id",<%=request.getParameter("id")%>);	
			requestV2(removePanier , "remove" , addform , loaderAddPanier , submitAddPanier , null , xhraddpanier );
		}
		
		e.preventDefault();
	}
	
	
	function addPanier(sData,Extension)
	{
		var obj = null;
		
		if(sData != null)
			obj = JSON.parse(sData);
		
		if(obj != null)
		{
			console.log(obj);
			if(obj == true)
			{
				submitAddPanier.id = "submitRemovePanier";
				loaderAddPanier.id = "loaderRemovePanier";
				change.innerHTML = "Remove";
				
				
				var nibbler = document.getElementById("nibbler");
				nibbler.innerHTML = parseInt(document.getElementById("nibbler").innerHTML) + 1;
			}
		}
	}
	
	function removePanier(sData,Extension)
	{
		var obj = null;
		
		if(sData != null)
			obj = JSON.parse(sData);
		
		if(obj != null)
		{
			console.log(obj);
			if(obj == true)
			{
				submitAddPanier.id = "submitAddPanier";
				loaderAddPanier.id = "loaderAddPanier";
				change.innerHTML = "Add to cart";
				
				var nibbler = document.getElementById("nibbler");
				nibbler.innerHTML = parseInt(document.getElementById("nibbler").innerHTML) - 1;
			}
		}
	}
	
</script>
<c:import url="footer.jsp"></c:import>