<c:import url="menubar.jsp"></c:import>

<main id="panier">	
	<c:if test="${ !empty nbele }">
		<c:choose>
			<c:when test="${ nbele > 0 }">
				<h3 id="h3t">Your cart : </h3>
				
				<c:if test="${ !empty error }">
					<c:choose>
						<c:when test="${ error == 'You must be login to confirm...click here to login' }">
							<p class="err" style="display:block;"><a style="color:#c86e6e;" id="logcart" href=""><c:out value="${ error }"></c:out></a></p>
						</c:when>
						<c:otherwise>
							<p class="err" style="display:block;"><c:out value="${ error }"></c:out></p>
						</c:otherwise>
					</c:choose>
				</c:if>
				<table id="tab" style="width:100%">
					<tr>
						<td class="bluetd">Picture</td>
						<td class="bluetd">Name</td>
						<td class="bluetd">Unit price</td>
						<td class="bluetd">Overall price</td>
						<td class="bluetd">Manage</td>
						<td class="bluetd">Remove</td>
					</tr>
					<c:forEach var="item" items="${ sessionScope.panier.getPanier() }">
						<tr class="item" id='<c:out value="trid-${ item.value.getId() }"></c:out>'>
							<td>
								<c:choose>
									<c:when test="${ !empty item.value.getImages() }">
										<img id="slider" style="max-width:80px;max-height:60px;min-height:60px;" src="/millionrama/productimg/<c:out value="${ item.value.getImages()[0].getId() }"></c:out>_ipid=<c:out value="${item.value.getImages()[0].getFileName() }"></c:out>"/>
									</c:when>
									<c:otherwise>
										<img id="slider" style="max-width:80px;max-height:60px;min-height:60px;" src="/millionrama/images/noimg.jpg"/>
									</c:otherwise>
								</c:choose>
							</td>
							<td><a href='<c:out value="p?id=${ item.value.getId() }"></c:out>' ><c:out value="${ item.value.getName() }"></c:out></a></td>
							<td><c:out value="${ item.value.getPrice() }"></c:out></td>
							<td><c:out value="${ item.value.getPrice() * item.value.getQuantity() }"></c:out></td>
							<td>

								<button id='<c:out value="itemdelete-${ item.value.getId() }"></c:out>'  >-</button>
								<input id='<c:out value="input-${ item.value.getId() }"></c:out>' type="text" value='<c:out value="${ item.value.getQuantity() }"></c:out>' />
								<button id='<c:out value="itemadd-${ item.value.getId() }"></c:out>'  >+</button>
							</td>
							<td><button id='<c:out value="itemremove-${ item.value.getId() }"></c:out>' >Remove</button></td>
						</tr>
					</c:forEach>
				</table>
				<p id="overall">Overall price : <span id="ovrprice"><c:out value="${ globalprice }$"></c:out></span></p>
				<form id="confirmform" action="cart" method="post">
					<input id="confirmbutton" type="submit" value="Next step"/>
				</form>
			</c:when>
			<c:otherwise>
				<p class="emptycart">Your cart is empty</p>
			</c:otherwise>
		</c:choose>
	</c:if>
</main>

<script src="${pageContext.request.contextPath}/script/ajax.js"></script>
<script>
	var tabitem = document.getElementById("tab").getElementsByTagName("button");
	var overall = document.getElementById("overall");
	var tabinp = document.getElementById("tab").getElementsByTagName("input");
	var xhraddanoter = null;
	var xhrdelanoter = null;
	var xhronchange = null;
	var xhrrem = null;
	
	if(document.getElementById("logcart") != null)
	{
		document.getElementById("logcart").onclick = function(e)
		{
			hidden.style.display = "block";
			loginRegister.style.display = "block";
			registerSection.style.display = "none";
			loginSection.style.display = "block";
			register.style.backgroundColor = "#0099FF";
			register.getElementsByTagName("a")[0].style.color = "white";	
			login.getElementsByTagName("a")[0].style.color = "#0099FF";	
			login.style.backgroundColor = "white";
			e.preventDefault();
		}
	}
	
	for(var i = 0 ; i <= tabinp.length-1 ; i++)
	{
		var split = tabinp[i].id.split("-");
		
		 tabinp[i].onchange = (function(id)
			        {
			            return function() 
			            {
			            	var fdo = new FormData();
			            	fdo.append("id",id);
			            	fdo.append("value",this.value);
			            	
			            	var td = this.parentNode.parentNode.getElementsByTagName("td")[3];
			            	var td2 = this.parentNode.parentNode.getElementsByTagName("td")[2];
			            	var input = document.getElementById("input-"+id);
							requestV22(readOnChange , "onchange" , fdo , null , xhronchange ,td,td2,input);
			            }
			        })(split[1]);
	}
	
	function readOnChange(sData,Extension,td,td2,input)
	{
		var obj = null;

		if(sData != null)
			obj = JSON.parse(sData);
		
		if(obj != null)
		{
			if(obj[0] == "true")
			{
				console.log(obj[1]);
				if(obj[1] == "addition")
				{
					var before = td.innerHTML; // on stock avant
					var value = parseFloat(td.innerHTML);
					var value2 = parseFloat(td2.innerHTML);
					input.value = parseInt(input.value);
					td.innerHTML = input.value * value2;
					var after = td.innerHTML; // on stock apres
					
					var difference = parseFloat(after) - parseFloat(before); //on calcule la difference
					
					
					
				 	var ovr = document.getElementById("ovrprice")
					current = parseFloat(ovr.innerHTML);
					var finalprice = current + difference;
					ovr.innerHTML = finalprice +"$";
				}
				else if(obj[1] == "soustraction")
				{
					var before = td.innerHTML; // on stock avant
					var value = parseFloat(td.innerHTML);
					var value2 = parseFloat(td2.innerHTML);
					input.value = parseInt(input.value);
					td.innerHTML = input.value * value2;
					var after = td.innerHTML; // on stock apres
					
					var difference = parseFloat(before) - parseFloat(after); //on calcule la difference
					
					var ovr = document.getElementById("ovrprice")
					current = parseFloat(ovr.innerHTML);
					var finalprice = current - difference;
					ovr.innerHTML = finalprice +"$";
				}
				else
				{
					var before = td.innerHTML; // on stock avant
					var value = parseFloat(td.innerHTML);
					var value2 = parseFloat(td2.innerHTML);
					input.value = 1;
					input.value = parseInt(input.value);
					td.innerHTML = input.value * value2;
					var after = td.innerHTML; // on stock apres
					
					var difference = parseFloat(before) - parseFloat(after); //on calcule la difference
					
					var ovr = document.getElementById("ovrprice")
					current = parseFloat(ovr.innerHTML);
					var finalprice = current - difference;
					ovr.innerHTML = finalprice +"$";
				}
			}
		}
	}
	
	function readAddOther(sData,Extension,td,td2,input)
	{
		var obj = null;

		if(sData != null)
			obj = JSON.parse(sData);
		
		if(obj != null)
		{
			if(obj == true)
			{
				var value = parseFloat(td.innerHTML);
				var value2 = parseFloat(td2.innerHTML);
				console.log(value2);
				input.value = parseInt(input.value)+1;
				td.innerHTML = value + value2 ;
				var ovr = document.getElementById("ovrprice")
				current = parseFloat(ovr.innerHTML);
				var finalprice = current + value2;
				ovr.innerHTML = finalprice +"$";
				console.log(finalprice);
			}
		}
	}
	
	function readDelOther(sData , Extension , td , td2 , input)
	{
		var obj = null;

		if(sData != null)
			obj = JSON.parse(sData);
		
		if(obj != null)
		{
			if(obj == true)
			{
				var value = parseFloat(td.innerHTML);
				var value2 = parseFloat(td2.innerHTML);
				console.log(value2);
				input.value = parseInt(input.value)-1;
				td.innerHTML = value - value2 ;
				var ovr = document.getElementById("ovrprice")
				current = parseFloat(ovr.innerHTML);
				var finalprice = current - value2;
				ovr.innerHTML = finalprice +"$";
				console.log(finalprice);
			}
		}
	}
	
	function readRemOther(sData , Extension , td , td2 , input)
	{
		var obj = null;

		console.log(sData);
		if(sData != null)
			obj = JSON.parse(sData);
		
		if(obj != null)
		{
			var ovr = document.getElementById("ovrprice");
			
			var nbRemove = parseFloat(td.innerHTML);
			var currentGlobal = parseFloat(ovr.innerHTML);
			var finalpriceg = currentGlobal - nbRemove;
			ovr.innerHTML = finalpriceg;
			
			var idd = td.parentNode.id;
			console.log(td.parentNode.parentNode.parentNode);
			td.parentNode.parentNode.removeChild(document.getElementById(idd));
			
			var nibbler = document.getElementById("nibbler");
			nibbler.innerHTML = parseInt(document.getElementById("nibbler").innerHTML) - 1;
			
			if((document.getElementById("tab").getElementsByTagName("tr").length-1) <= 0)
			{
				document.getElementById("confirmform").removeChild(document.getElementById("confirmbutton"));
				document.getElementById("overall").style.display = "none";
				document.getElementById("tab").style.display = "none";
				document.getElementById("h3t").style.display = "none";
				var p = document.createElement("p");
				p.innerHTML = "You cart is empty";
				p.className = "emptycart";
				
				document.getElementById("panier").appendChild(p);
			}
		}
	}
	
	for(var i = 0 ; i <= tabitem.length-1 ; i++)
	{
		var split = tabitem[i].id.split("-");
		
		if(tabitem[i].id.includes("itemadd"))
		{				
	        tabitem[i].onclick = (function(id)
			        {
			            return function() 
			            {
			            	var fd = new FormData();
			            	fd.append("id" , id);
			            	var td = this.parentNode.parentNode.getElementsByTagName("td")[3];
			            	var td2 = this.parentNode.parentNode.getElementsByTagName("td")[2];
			            	var input = document.getElementById("input-"+id);
							requestV22(readAddOther , "test" , fd , null , xhraddanoter ,td,td2,input);
			            }
			        })(split[1]);
		}
		else if(tabitem[i].id.includes("itemdelete"))
		{		
	        tabitem[i].onclick = (function(id)
			        {
			            return function() 
			            {
			            	var fd = new FormData();
			            	fd.append("id" , id);
			            	var td = this.parentNode.parentNode.getElementsByTagName("td")[3];
			            	var td2 = this.parentNode.parentNode.getElementsByTagName("td")[2];
			            	var input = document.getElementById("input-"+id);
							requestV22(readDelOther , "removeone" , fd , null , xhrdelanoter ,td,td2,input);
			            }
			        })(split[1]);
		}
		else if(tabitem[i].id.includes("itemremove"))
		{
	        tabitem[i].onclick = (function(id)
			        {
			            return function() 
			            {
			            	var fr = new FormData();
			            	fr.append("id" , id);
			            	var td = this.parentNode.parentNode.getElementsByTagName("td")[3];
			            	var td2 = this.parentNode.parentNode.getElementsByTagName("td")[2];
			            	var input = document.getElementById("input-"+id);
			            	requestV22(readRemOther , "remove" , fr , null , xhrrem ,td,td2,input);            	
			            }
			        })(split[1]);
		}
	}

</script>
<c:import url="footer.jsp"></c:import>