<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"  href="<c:url value='style/style.css'/>"/> 
<link rel="stylesheet" type="text/css"  href="<c:url value='style/contact.css'/>"/> 
<title>Insert title here</title>
</head>
<body>
	<menu>
		<section id="globalmenusection">
			<section id="logosection"><h4>Millonrama </h4></section>
			<section id="searchbarsection">
                <form action="product" method="get"> 
					<input type="text" name="searchname" placeholder="Search for a product..."/>
					<select name="searchid" id="categoryselect" style="">
						<option value="0">All Categories</option>
                                              
						<c:forEach var="item" items="${ categories }">
							<option value="<c:out value="${ item.id }"></c:out>" ><c:out value="${ item.getName() }"></c:out></option>
						</c:forEach>
					</select>
					<input type="submit" id="search" value="search">
				</form>
			</section>
			<section id="optionsection">
				
				<c:choose>
					<c:when test="${ !empty sessionScope.user }">
						<p>
							<a href=""><c:out value="${ sessionScope.user.getFirstname() }"></c:out></a>
						</p>
						<ul id="boom">
							<li class="allo"><a href="setting">Settings</a></li>
							<c:if test="${ sessionScope.user.getRole().getName() == 'admin' }">
								<li><a href="admin">Administration</a></li>
							</c:if>
							<li><a href="disconnect">Disconnect</a></li>
						</ul>
					</c:when>	
					<c:otherwise>
						<p><a href="">Sign in</a></p>
					</c:otherwise>
				</c:choose>
				
				<p id="imgpanier">
					<a href="cart">
						<img src="http://www.orangeandcoco.com/bundles/panierdachattemplateengine/devise/images/panier.png" alt="panier" />
						<span id="nibbler"><c:out value="${ nbele }"></c:out></span>
					</a>
				</p>
			</section>
		</section>
	</menu>
	
	<div id="menubar-2">
	<ul>
		<li> <a href="index">Accueil </a> </li>
		<li> <a href="contact">Contact </a> </li>
	</ul>
	
	
	</div>
	
	<% 
		/*
			script pour quand lutilisateur est connecter elle gere notament
			la barre doption quand on cliquer sur le nom de lutilisateur
		*/
	%>
	<c:if test="${ !empty sessionScope.user }">
		<script src="${pageContext.request.contextPath}/script/optionsection.js"></script>
	</c:if>
	
	<% 
		/*
			va apparaitre suelement quand lutilisateur nes pas
			connecter ses la fenetre cacher qui contient le login
			et le register
		*/
	%>
	<c:if test="${ empty sessionScope.user }">
		<section id="hidden"></section>	
		<section id="loginRegister">
			<section>	
				<section>
					<p id="login"><a href="">Login</a></p>
					<p id="register"><a href="">Register</a></p>
				</section>
				<p id="close"><a href="">X</a></p>
			</section>
			<section id="loginsection">	
				<form>
					<p id="EmailloginErr" class="err"></p>
					<input class="loginRegisterInput" type="text" placeholder="Email" id="emaillogin"/>
					<input class="loginRegisterInput" type="password" placeholder="Password" id="loginpassword"/>
					<a href="" style="color:white;"><div style="text-align:center;margin-top:10px;width:100px;background-color:#0099FF;display:flex;padding:5px;" id="submitlogin">
						<p style="flex:1;padding-top:5px;padding-bottom:5px;">Connect</p>
						<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderlogin"/>
					</div></a>
				</form>
			</section>
			<section id="registerSection">		
				<form>
					<p id="FirstnameregisterErr" class="err"></p>
					<input class="loginRegisterInput" type="text" placeholder="Firstname" id="fname"/>
					<p id="LastnameregisterErr" class="err"></p>
					<input class="loginRegisterInput" type="text" placeholder="Lastname" id="lname"/>
					<p id="EmailregisterErr" class="err"></p>
					<input class="loginRegisterInput" type="text" placeholder="Email" id="emailr"/>
					<p id="PasswordregisterErr" class="err"></p>
					<input class="loginRegisterInput" type="password" placeholder="Password" id="pwr"/>
					<p id="PasswordConfirmationregisterErr" class="err"></p>
					<input class="loginRegisterInput" type="password" placeholder="Password confirmation" id="pwcr"/>
					<p id="G_recaptcha_responseregisterErr" class="err"></p>
					<div style="margin-top:10px;" class="g-recaptcha" data-sitekey="6LeXYQkUAAAAAMdY74ARNkJBxvOXva1yP8ufMtDQ"></div>
					<a href="" style="color:white;"><div style="text-align:center;margin-top:10px;width:100px;background-color:#0099FF;display:flex;padding:5px;" id="submitregister">
						<p style="flex:1;padding-top:5px;padding-bottom:5px;">Register</p>
						<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loader"/>
					</div></a>
				</form>
			</section>
		</section>
	</c:if>

	<% 
		/*
			va apparaitre suelement quand lutilisateur nes pas
			connecter ses le script qui permet de pop la fenetre 
			cacher de register et de login
		*/
	%>	
<c:if test="${ empty sessionScope.user }">
	<script src='https://www.google.com/recaptcha/api.js'></script>	
	<script>
		var signin = document.getElementById("optionsection").getElementsByTagName("p")[0];
		var login = document.getElementById("login");
		var register = document.getElementById("register")
		var loginSection = document.getElementById("loginsection")
		var registerSection = document.getElementById("registerSection");
		var hidden = document.getElementById("hidden");
		var submit = document.getElementById("submitregister");
		var loader = document.getElementById("loader");
		var loaderlogin = document.getElementById("loaderlogin");
		var submitlogin = document.getElementById("submitlogin");
		
		register.onclick = function(e)
		{
			e.preventDefault()
			register.getElementsByTagName("a")[0].style.color = "#0099FF";
			login.getElementsByTagName("a")[0].style.color = "white";
			register.style.backgroundColor = "white";
			login.style.backgroundColor = "#0099FF";
			loginSection.style.display = "none";
			registerSection.style.display = "block";
		}
		
		login.onclick = function(e)
		{
			e.preventDefault()
			register.getElementsByTagName("a")[0].style.color = "white";
			login.getElementsByTagName("a")[0].style.color = "#0099FF";
			register.style.backgroundColor = "#0099FF";
			login.style.backgroundColor = "white";
			loginSection.style.display = "block";
			registerSection.style.display = "none";
		}
		
		signin.onclick = function(e)
		{
			hidden.style.display = "block";
			loginRegister.style.display = "block";
			registerSection.style.display = "none";
			loginSection.style.display = "block";
			register.style.backgroundColor = "#0099FF";
			register.getElementsByTagName("a")[0].style.color = "white";	
			login.getElementsByTagName("a")[0].style.color = "#0099FF";	
			login.style.backgroundColor = "white";
			e.preventDefault();
		}
		
		hidden.onclick = function(e)
		{
			loginRegister.style.display = "none";
			hidden.style.display = "none";
		}
		
		submitregister.onclick = function(e)
		{
			var fname = document.getElementById("fname").value;	
			var lname = document.getElementById("lname").value;	
			var emailr = document.getElementById("emailr").value;	
			var pwr = document.getElementById("pwr").value;	
			var pwcr = document.getElementById("pwcr").value;	
			
			request(readData , "index" , "Firstname="+fname+"&Lastname="+lname+"&Email="+emailr+"&Password="+pwr+"&PasswordConfirmation="+pwcr+"&G_recaptcha_response="+grecaptcha.getResponse(),loader,submit , "register");
			e.preventDefault();
		}
		
		submitlogin.onclick = function(e)
		{
			var emaillogin = document.getElementById("emaillogin").value;
			var pwlogin = document.getElementById("loginpassword").value;
			
			request(readData,"login" , "Email="+emaillogin+"&Password="+pwlogin , loaderlogin , submitlogin , "login");
			
			e.preventDefault();
		}
		
		function getXMLHttpRequest()
		{
			if(window.XMLHttpRequest || window.ActiveXObject)
			{
				if(window.ActiveXObject)
				{
					try
					{
						return new ActiveXObject("Msxml2.XMLHTTP");
					}
					catch(e)
					{
						return new ActiveXObject("Microsoft.XMLHTTP");
					}
				}
				else
					return new XMLHttpRequest();
			}
			else
				return null;
		}
		
		var xhr = null;
		
		//fonction qui envoie la requete ajax
		function request(callback , url , strPostVar , imgloaderId , divButtonId , Extension)
		{
			 if (xhr && xhr.readyState != 0) 
			       xhr.abort();
			    
			var xhr = getXMLHttpRequest();
			
			xhr.onreadystatechange = function(e)
			{
				if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
				{
					console.log(xhr.responseText);
					callback(xhr.responseText , Extension);
					imgloaderId.style.display = "none";
					divButtonId.style.backgroundColor = "#0099FF";
					divButtonId.style.color = "white";
					divButtonId.style.border = "border 1px solid black";
				}
				else if (xhr.readyState < 4) 
				{
					imgloaderId.style.display = "inline";
					divButtonId.style.backgroundColor = "#99D6FF";
					divButtonId.style.color = "white";
					divButtonId.style.border = "border 1px solid black";
				}
			}
			
			xhr.open("POST", url , true);
			xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.send(strPostVar);
		}
		
		var arrayIdToClear = [];
		
		function readData(sData , idExtension)
		{
			console.log(sData);
			for(var i = 0 ; i <= arrayIdToClear.length-1 ; i++)
			{
				var todelete = document.getElementById(arrayIdToClear[i]+"Err");
				if(todelete != null)
					{
						todelete.innerHTML = "";
						todelete.style.display = "none";
					}
			}
			
			arrayIdToClear.splice(0,arrayIdToClear.length);
			
			if(sData != "" && sData != null )
			{
				var obj = JSON.parse(sData);
				
				if(obj[0].name == null)
					window.location = "/millionrama/"+obj;
				
				for(var i = 0 ; i <= obj.length-1;i++)
				{
					var err = document.getElementById(obj[i].name+idExtension+"Err");
			
					if(obj[i].msg != null)
					{
						err.innerHTML = obj[i].msg;	
						err.style.display = "block";
						err.className = "err";
						grecaptcha.reset();
					}
					else
					{
						if(obj[i].validMsg != null)
						{
							if(err != null)
							{
								err.innerHTML = obj[i].validMsg;
								err.style.display = "block";
								err.className = "valid";
							}
						}
					}
					
					arrayIdToClear.push(obj[i].name+idExtension);
				}
			}
			else
			{
				window.location = "/millionrama/index";
			}
		}
	
		
	</script>
</c:if>