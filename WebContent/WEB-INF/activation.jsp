<c:import url="menubar.jsp"></c:import>

<main>
	<h1 class="underlineh1">Your account have been activated!</h1>
	<c:choose>
		<c:when test="${ !empty user }">
		<section>
				<p>
					Hi <c:out value="${ user.getFirstname()}"></c:out> <c:out value="${ user.getLastname()}"></c:out>
					you can now use you email : <c:out value="${ user.getEmail() }"></c:out> to connect.
				</p>
				<p>You will be redirect in 5 seconde <a class="bluehref" href="login">click here</a> if the redirect dont work</p>
			</section>
		</c:when>
	</c:choose>
	
</main>

<script>
	<% //on redirige lutilisateur vers la page login apres 5 seconde %>
	function redirect() 
	{
	    location.href = "/millionrama/index";
	}
	window.setTimeout("redirect()", 5000);
</script>
<c:import url="footer.jsp"></c:import>