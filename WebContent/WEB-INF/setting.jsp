<c:import url="menubar.jsp"></c:import>

<main id="mainadmin">
	<h1>Setting :</h1>
	
		<form class="adminsection" method="post" action="setting">
		<h3>Edit profil</h3>
		
		<p id="OldpasswordEditUserErr" class="err"></p>
		<p><label for="Oldpassword">Old password :</label></p>
		<p><input class="loginRegisterInput" type="password" name="Password" id="Oldpassword" /></p>
		
		<p id="PasswordEditUserErr" class="err"></p>
		<p><label for="Password">Password :</label></p>
		<p><input class="loginRegisterInput" type="password" name="Password" id="Password" /></p>
		
		<p id="PasswordConfirmationEditUserErr" class="err"></p>
		<p><label for="PasswordConfirmation">Password confirmation :</label></p>
		<p><input class="loginRegisterInput" type="password" name="PasswordConfirmation" id="PasswordConfirmation" /></p>
		
		<p id="EmailEditUserErr" class="err"></p>
		<p><label for="Email">Email :</label></p>
		
		<c:choose>
			<c:when test="${ !empty sessionScope.user.getEmail() }">
				<p><input class="loginRegisterInput" type="email" name="Email" id="Email" value="<c:out value="${sessionScope.user.getEmail()}"></c:out>" /></p>
			</c:when>
			<c:otherwise>
				<p><input class="loginRegisterInput" type="email" name="Email" id="Email" /></p>
			</c:otherwise>
		</c:choose>
		
		<a href="" style="color:white;">
			<div style="text-align:center;margin-top:10px;width:100px;background-color:#0099FF;display:flex;padding:5px;" id="submitEditUser">
				<p style="flex:1;padding-top:5px;padding-bottom:5px;">Update</p>
				<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderEditUser"/>
			</div>
		</a>
	</form>
	
	<form class="adminsection" method="post" action="setting">
		<h3>Manage credit card</h3>
		
		<p id="NameOnCardAddCardErr" class="err"></p>
		<p><label for="NameOnCard">Name on card :</label></p>
		<p><input class="loginRegisterInput" type="text" id="NameOnCard" /></p>
		
		<p id="CardNumberAddCardErr" class="err"></p>
		<p><label for="CardNumber">Card number :</label></p>
		<p><input class="loginRegisterInput" type="text" id="CardNumber" /></p>
		
		<p id="CardtypeAddCardErr" class="err"></p>
		<p><label for="Cardtype">Select card type :</label></p>
		<p>
			<select id="Cardtype">
				<option value="Mastercard">Mastercard</option>
				<option value="Visa">Visa</option>
				<option value="American express">American express</option>
			</select>
		</p>
		
		<a href="" style="color:white;">
			<div style="text-align:center;margin-top:10px;width:100px;background-color:#0099FF;display:flex;padding:5px;" id="submitAddCard">
				<p style="flex:1;padding-top:5px;padding-bottom:5px;">Add</p>
				<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderAddCard"/>
			</div>
		</a>
		<table id="ccmanage" <c:if test="${ empty cclist }">style="display:none;"</c:if>>
			<tr>
				<td class="bluetd">Type</td>
				<td class="bluetd">Name</td>
				<td class="bluetd">Card number</td>
			</tr>
			<c:if test="${ !empty cclist }">
					<c:forEach var="item" items="${ cclist }">
						<tr>
							<td class="whitetd"><c:out value="${ item.getType() }"></c:out></td>
							<td class="whitetd"><c:out value="${ item.getName() }"></c:out></td>
							<td class="whitetd"><c:out value="${ item.getCardnumber() }"></c:out></td>
						</tr>
					</c:forEach>
			</c:if>
		</table>
		
	</form>
	
</main>

<script src="${pageContext.request.contextPath}/script/ajax.js"></script>
<script src="${pageContext.request.contextPath}/script/settingpage.js"></script>
<c:import url="footer.jsp"></c:import>