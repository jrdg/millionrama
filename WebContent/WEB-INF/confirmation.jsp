<c:import url="menubar.jsp"></c:import>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<main id="mainconfirm">
	<table id="tabconfirm">
		<tr>
			<td class="bluetd">Name</td>
			<td class="bluetd">Quantity</td>
			<td class="bluetd">Total</td>
		</tr>
		<c:forEach var="item" items="${ sessionScope.panier.getPanier() }">
			<tr>
				<td><a style="color:#008AE6;text-decoration:underline;" href='<c:out value="p?id=${ item.value.getId() }"></c:out>' ><c:out value="${ item.value.getName() }"></c:out></a></td>
				<td><c:out value="${ item.value.getQuantity() }"></c:out></td>
				<td><c:out value="${ item.value.getPrice() * item.value.getQuantity() }"></c:out></td>
			</tr>
		</c:forEach>
	</table>
	<p id="overall">Overall price : <span id="ovrprice"><c:out value="${ globalprice }$"></c:out></span></p>
	
	<section id="globalconfirm">
		<section id="existing"  class="confirmsection">
			<h3 style="margin-bottom:10px;">Credit card information and PAY</h3>
			<c:choose>
				<c:when test="${ !empty clist }">
					<c:choose>
						<c:when test="${ fn:length(clist) > 0 }">
						<form>
							<p><label>Choose a existing card :</label></p>
							<p id="CreditCardIdexistingErr" class="err"></p>
							<select id="CreditCardIdexisting">
								<c:forEach var="item" items="${ clist }">
									<option value="<c:out value="${ item.getId() }"></c:out>">
										<c:out value="${ item.getName() } : "></c:out>
										<c:out value="${ item.getCardnumber() }"></c:out>
									</option>
								</c:forEach>
							</select>
							<p id="CvvexistingErr" class="err"></p>
							<p><label>Cvv :</label></p>
							<p><input id="Cvvexisting" type="text" placeholder="CVV"/></p>
							<p><label>Expiration date :</label></p>
							<p>
								<p id="MonthexistingErr" class="err"></p>
								<input type="text" id="Monthexisting" placeholder="Month"/>
								<p id="YearexistingErr" class="err"></p>
								<input type="text" placeholder="Year" id="Yearexisting"/>
							</p>
							<a href="" style="color:white;">
								<div style="text-align:center;margin-top:10px;width:100px;background-color:#0099FF;display:flex;padding:5px;" id="submitExistingCc">
									<p style="flex:1;padding-top:5px;padding-bottom:5px;">Pay</p>
									<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderExistingCc"/>
								</div>
							</a>
						</form>
						</c:when>
						<c:otherwise>
							You do have have any credit card registred
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					You do have have any credit card registred <a href="setting" style="color:#008AE6;text-decoration:underline;">click here</a> to add one
				</c:otherwise>
			</c:choose>
		</section>
	</section>
	
</main>

<script src="${pageContext.request.contextPath}/script/ajax.js"></script>
<script>
	var exist = document.getElementById("existing");
	var newc = document.getElementById("newcredit");
	
	//ajax du existing
	var submitExist = document.getElementById("submitExistingCc");
	var loaderExist = document.getElementById("loaderExistingCc");
	var ccidExist = document.getElementById("CreditCardIdexisting");
	var cvvExist = document.getElementById("Cvvexisting");
	var monthExist = document.getElementById("Monthexisting");
	var yearExist = document.getElementById("Yearexisting");
	var xhrexist = null;
	
	
	submitExist.onclick = function(e)
	{
		console.log("allo");
		
		var form = new FormData();
		form.append("CreditCardId",ccidExist.value);
		form.append("Cvv",cvvExist.value);
		form.append("Month",monthExist.value);
		form.append("Year",yearExist.value);
	
		
		requestV2(readExisting , "payexisting" , form , loaderExist , submitExist , "existing" , xhrexist );
		e.preventDefault();
	}
	
	var arrayIdToClear = [];
	
	function readExisting(sData , idExtension)
	{
		console.log(sData);
		for(var i = 0 ; i <= arrayIdToClear.length-1 ; i++)
		{
			var todelete = document.getElementById(arrayIdToClear[i]+"Err");
			if(todelete != null)
				{
					todelete.innerHTML = "";
					todelete.style.display = "none";
				}
		}
		
		arrayIdToClear.splice(0,arrayIdToClear.length);
		
		if(sData != "" && sData != null )
		{
			var obj = JSON.parse(sData);
			console.log(obj);
			
			var valid = true;
			
			for(var i = 0 ; i <= obj.length-1;i++)
			{
				var err = document.getElementById(obj[i].name+idExtension+"Err");
		
				if(obj[i].msg != null)
				{
					valid = false;
					err.innerHTML = obj[i].msg;	
					err.style.display = "block";
				}
				
				arrayIdToClear.push(obj[i].name+idExtension);
			}
			
			if(valid)
			{
				console.log("finishhhhhhhh");
				window.location.href = "/millionrama/finish";
			}
		}
	}
	
</script>
<c:import url="footer.jsp"></c:import>
