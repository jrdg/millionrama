<c:import url="menubar.jsp"></c:import>

<main>

<h1 class="underlineh1">Login</h1>

<section id="loginpagefirstsection">
	<form id="loginform" action="login" method="post">
	
		<c:if test="${ !empty helper }">
			<p id="EmailErr" class="err">
				<c:out value="${ helper.getErrorMsg('Email') }"></c:out>
			</p>
		</c:if>
		
		<p><input type="text" name="Email" placeholder="Email"/></p>
		<p><input type="password" name="Password" placeholder="Password"/></p>
		<p><input type="submit" value="Connect" /></p>	
	</form>
	<section>
		
	</section>
</section>

</main>

<c:import url="footer.jsp"></c:import>