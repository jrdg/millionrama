<c:import url="menubar.jsp"></c:import>

<main id="mainadmin">
	<h1>Adminstration :</h1>
	<form class="adminsection">
		<h3>Add categorie</h3>
		
		<p id="NameaddcatErr" class="err"></p>
		<p><label for="categorieName">Name :</label></p>
		<p><input type="text" name="Nameaddcat" id="categorieName"/></p>
		<a href="" style="color:white;">
			<div style="text-align:center;margin-top:10px;width:100px;background-color:#0099FF;display:flex;padding:5px;" id="submitAddCategorie">
				<p style="flex:1;padding-top:5px;padding-bottom:5px;">Add</p>
				<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderAddCategorie"/>
			</div>
		</a>
	</form>
	
		
	<form class="adminsection">
		<h3>Add a product</h3>
		
		<p id="NameaddproductErr"></p>
		<p><label for="productName">Name :</label></p>
		<p><input type="text" name="productName" id="productName" /></p>
		
		<p id="DescriptionaddproductErr"></p>
		<p><label for="productDescription">Description :</label></p>
		<p><input type="text" name="productDescription" id="productDescription" /></p>
		
		<p id="PriceaddproductErr"></p>
		<p><label for="productPrice">Price :</label></p>
		<p><input type="text" name="productPrice" id="productPrice" /></p>
		
		<p id="QtyaddproductErr"></p>
		<p><label for="productQty">Quantity :</label></p>
		<p><input type="text" name="productQty" id="productQty" /></p>
		
		<p id="CategorienameaddproductErr"></p>
		<p><label for="productCategoriename">Categorie :</label></p>
		<p>
			<select name="productCategoriename" id="productCategoriename">
				<option selected disabled>Select a categorie</option>
				<c:forEach var="item" items="${ categories }">
					<option value="<c:out value="${ item.getName() }"></c:out>" ><c:out value="${ item.getName() }"></c:out></option>
				</c:forEach>
			</select>
		</p>
		
		<a href="" style="color:white;">
			<div style="text-align:center;margin-top:10px;width:100px;background-color:#0099FF;display:flex;padding:5px;" id="submitAddProduct">
				<p style="flex:1;padding-top:5px;padding-bottom:5px;">Add</p>
				<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderAddProduct"/>
			</div>
		</a>
	</form>
	
	<section id="manageProductSection" class="adminsectionmanager">
		<h3>Manage products</h3>
		<div>
			<p id="CategorienameproductmanageErr"></p>	
			<section>		
				<section>
					<select name="manageCategoriename" id="productManageCategorie">
							<option value="All" selected>All categorie</option>
							<c:forEach var="item" items="${ categories }">
								<option value="<c:out value="${ item.getName() }"></c:out>" ><c:out value="${ item.getName() }"></c:out></option>
							</c:forEach>
					</select>	
					<input type="text" placeholder="Product name" id="productManageName"/>
				</section>
				
				<section>
					<a  style="color:white;">
						<div style="text-align:center;margin-top:10px;width:100px;background-color:#0099FF;display:flex;padding:5px;" id="submitstartproduct">
							<p style="flex:1;padding-top:5px;padding-bottom:5px;">Start</p>
							<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderStartProduct"/>
						</div>
					</a>
				</section>
			</section>
			<section id="interfacemanageproduct">
					<table id="interfacemanageproducttable">
					</table>
					<h5 id="inttitle">fill the form and press start</h5>
			</section>			
			<a  style="color:white;">
				<div style="text-align:center;background-color:#0099FF;display:flex;padding:5px;" id="submitDeleteProduct">
					<p style="flex:1;padding-top:5px;padding-bottom:5px;">Delete</p>
					<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderDeleteProduct"/>
				</div>
			</a>
		</div>
	</section>
	
	<section id="hiddenedit"></section>
	<section id="hiddeneditform">
		<h3>Edit a product</h3>
		
		<form enctype="multipart/form-data">
			
			<p id="NameeditproductErr"></p>
			<p><label for="productEditName">Name :</label></p>
			<p><input type="text" name="productEditName" id="productEditName" /></p>
			
			<p id="DescriptioneditproductErr"></p>
			<p><label for="productEditDescription">Description :</label></p>
			<p><input type="text" name="productEditDescription" id="productEditDescription" /></p>
			
			<p id="PriceeditproductErr"></p>
			<p><label for="productEditPrice">Price :</label></p>
			<p><input type="text" name="productEditPrice" id="productEditPrice" /></p>
			
			<p id="QtyeditproductErr"></p>
			<p><label for="productEditQty">Quantity :</label></p>
			<p><input type="text" name="productEditQty" id="productEditQty" /></p>
			
			<p id="CategorienameeditproductErr"></p>
			<p><label for="productEditCategoriename">Categorie :</label></p>
			<p>
				<select name="productEditCategoriename" id="productEditCategoriename">
					<option selected disabled>Select a categorie</option>
					<c:forEach var="item" items="${ categories }">
						<option value="<c:out value="${ item.getName() }"></c:out>" ><c:out value="${ item.getName() }"></c:out></option>
					</c:forEach>
				</select>
			</p>
			
			<p id="PartErreditproductErr"></p>
			<p><label for="productEditPart">Image :</label></p>
			<p><input type="file" name="productEditPart" id="productEditPart" /></p>
			
			<input type="hidden" id="productEditId" />		
			<a href="" style="color:white;">
				<div style="text-align:center;margin-top:10px;width:100px;background-color:#0099FF;display:flex;padding:5px;" id="submitEditProduct">
					<p style="flex:1;padding-top:5px;padding-bottom:5px;" class="pinloader">Edit</p>
					<img style="flex:1;max-width:30px;height:30px;display:none;" src="img/ring-alt.gif" id="loaderEditProduct"/>
				</div>
			</a>
		
		</form>
		
		<section id="productImgSection">
		
		</section>
		
	</section>
	
</main>

<script src="${pageContext.request.contextPath}/script/ajax.js"></script>
<script src="${pageContext.request.contextPath}/script/adminpage.js"></script>