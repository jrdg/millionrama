/**
 * 
 */

//retourne la bonne instance lobjet XMLHttpRequest
function getXMLHttpRequest()
{
	if(window.XMLHttpRequest || window.ActiveXObject)
	{
		if(window.ActiveXObject)
		{
			try
			{
				return new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch(e)
			{
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
		else
			return new XMLHttpRequest();
	}
	else
		return null;
}




/*
 * NE PAS OUBLIER DINSTACIER UNE VARIABLE GLOBAL XHR COMME CECI : var xhr = null;
 * fonction qui envoie la requete ajax
 * CALLBACK : la fonction de retour qui jouera le role decider quoi faire avec le json recu
 * URL :  est lurl de la servlet
 * STRPOSTVAR :  est une chaine de charactere wui contient les parametre et leur valeur a envoyer en post
 * IMGLOADERID : est le document.getelementbyid du spinner loader du bouton assosier a la requete
 * DIVBUTTONID : est le document.getelementbyid qui conteitn le imgloaderid
 * EXTENSION :  est lextension quon donne au id exemple FirstnameloginErr lextension est login. ses pour differencier les different formulaire de chaque page
 */
function request(callback , url , strPostVar , imgloaderId , divButtonId , Extension)
{
	 if (xhr && xhr.readyState != 0) 
	 {
		 xhr.abort();
		 console.log("on annule");
     }
	    
	xhr = getXMLHttpRequest();
	
	xhr.onreadystatechange = function(e)
	{
		if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
		{
			callback(xhr.responseText , Extension);
			imgloaderId.style.display = "none";
			divButtonId.style.backgroundColor = "#0099FF";
			divButtonId.style.color = "white";
			divButtonId.style.border = "border 1px solid black";
		}
		else if (xhr.readyState < 4) 
		{
			imgloaderId.style.display = "inline";
			divButtonId.style.backgroundColor = "#99D6FF";
			divButtonId.style.color = "white";
			divButtonId.style.border = "border 1px solid black";
		}
	}
	
	xhr.open("POST", url , true);
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send(strPostVar);
}

function requestV2(callback , url , formdata , imgloaderId , divButtonId , Extension , xhr )
{
	 if (xhr && xhr.readyState != 0) 
	 {
		 xhr.abort();
		 console.log("on annule");
     }
	    
	xhr = getXMLHttpRequest();
	
	xhr.onreadystatechange = function(e)
	{
		if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
		{
			callback(xhr.responseText , Extension);
			imgloaderId.style.display = "none";
			divButtonId.style.backgroundColor = "#0099FF";
			divButtonId.style.color = "white";
			divButtonId.style.border = "border 1px solid black";
		}
		else if (xhr.readyState < 4) 
		{
			imgloaderId.style.display = "inline";
			divButtonId.style.backgroundColor = "#99D6FF";
			divButtonId.style.color = "white";
			divButtonId.style.border = "border 1px solid black";
		}
	}
	
	xhr.open("POST", url , true);
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.send(formdata);
}

function deleteImg(callback,url,formdata,imgloader , containerP)
{
	 if (xhrdeleteimg && xhrdeleteimg.readyState != 0) 
	 {
		 xhrdeleteimg.abort();
		 imgloader.style.display = "none";
		 console.log("on annule");
     }
	
	xhrdeleteimg = getXMLHttpRequest();
	
	xhrdeleteimg.onreadystatechange = function(e)
	{
		if(xhrdeleteimg.readyState == 4 && (xhrdeleteimg.status == 200 || xhrdeleteimg.status == 0))
		{
			callback(xhrdeleteimg.responseText,containerP);
			imgloader.style.display = "none";
		}
		else if (xhrdeleteimg.readyState < 4) 
			imgloader.style.display = "inline";
	}
	
	xhrdeleteimg.open("POST", url , true);
	xhrdeleteimg.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhrdeleteimg.send(formdata);
}

//requete pour envoyer un part dans la page admin
function partrequest(callback , url , formData , imgloaderId , divButtonId , Extension)
{
	 if (xhr && xhr.readyState != 0) 
	 {
		 xhr.abort();
		 console.log("on annule");
	 }
	 
	xhr = getXMLHttpRequest();
	
	xhr.onreadystatechange = function(e)
	{
		if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
		{
			callback(xhr.responseText , Extension);
			imgloaderId.style.display = "none";
			divButtonId.style.backgroundColor = "#0099FF";
			divButtonId.style.color = "white";
			divButtonId.style.border = "border 1px solid black";
		}
		else if (xhr.readyState < 4) 
		{
			imgloaderId.style.display = "inline";
			divButtonId.style.backgroundColor = "#99D6FF";
			divButtonId.style.color = "white";
			divButtonId.style.border = "border 1px solid black";
		}
	}
	
	xhr.open("POST", url , true);
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.send(formData);
}

//requete pour aller chercher les images 
function getImgrequest(callback , url , formData)
{
	 if (xhrimg && xhrimg.readyState != 0) 
	 {
		 xhrimg.abort();
		 console.log("on annule");
	 }
	    
	xhrimg = getXMLHttpRequest();
	
	xhrimg.onreadystatechange = function(e)
	{
		if(xhrimg.readyState == 4 && (xhrimg.status == 200 || xhrimg.status == 0))
		{
			callback(xhrimg.responseText);
			he.style.display = "block";
			hef.style.display = "block";
		}
	}
	
	xhrimg.open("POST", url , true);
	xhrimg.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhrimg.send(formData);
}

function deleteRequest(callback , url , strPostVar , imgloaderId , divButtonId)
{
	 if (xhrdelete && xhrdelete.readyState != 0) 
	 {
		 xhrdelete.abort();
		 console.log("on annule");
     }
	    
	xhrdelete = getXMLHttpRequest();
	
	xhrdelete.onreadystatechange = function(e)
	{
		if(xhrdelete.readyState == 4 && (xhrdelete.status == 200 || xhrdelete.status == 0))
		{
			callback(xhrdelete.responseText);
			imgloaderId.style.display = "none";
			divButtonId.style.backgroundColor = "#0099FF";
			divButtonId.style.color = "white";
			divButtonId.style.border = "border 1px solid black";
		}
		else if (xhrdelete.readyState < 4) 
		{
			imgloaderId.style.display = "inline";
			divButtonId.style.backgroundColor = "#99D6FF";
			divButtonId.style.color = "white";
			divButtonId.style.border = "border 1px solid black";
		}
	}
	
	xhrdelete.open("POST", url , true);
	xhrdelete.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhrdelete.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhrdelete.send(strPostVar);
}

function requestV22(callback , url , formdata , Extension , xhr ,td,td2,input )
{
	 if (xhr && xhr.readyState != 0) 
	 {
		 xhr.abort();
		 console.log("on annule");
     }
	    
	xhr = getXMLHttpRequest();
	
	xhr.onreadystatechange = function(e)
	{
		if(xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0))
			callback(xhr.responseText , Extension , td,td2,input);
	}
	
	xhr.open("POST", url , true);
	xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	xhr.send(formdata);
}
