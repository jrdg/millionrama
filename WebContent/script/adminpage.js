	//variable ajout dune categorie
	var submitAddCategorie = document.getElementById("submitAddCategorie");
	var categorieName = document.getElementById("categorieName");
	var loaderAddCategorie = document.getElementById("loaderAddCategorie");
	var categories = document.getElementById("categoryselect");
	
	//variable ajout dun produit
	var submitAddProduct = document.getElementById("submitAddProduct");
	var productName = document.getElementById("productName");
	var productDescription = document.getElementById("productDescription");
	var productPrice = document.getElementById("productPrice");
	var productCategoriename = document.getElementById("productCategoriename")
	var productQty = document.getElementById("productQty");
	var loaderAddProduct = document.getElementById("loaderAddProduct");
	var pc = document.getElementById("productCategoriename");
	
	//variable management product
	var submitStartProduct = document.getElementById("submitstartproduct");
	var loaderManageProduct = document.getElementById("loaderStartProduct");
	var productManageName = document.getElementById("productManageName");
	var productManageCategorie = document.getElementById("productManageCategorie");
	
	//variable edit produit
	var productEditName = document.getElementById("productEditName");
	var productEditDescription = document.getElementById("productEditDescription");
	var productEditPrice = document.getElementById("productEditPrice");
	var productEditCategoriename = document.getElementById("productEditCategoriename")
	var productEditQty = document.getElementById("productEditQty");
	var productEditId = document.getElementById("productEditId");
	var submitEditProduct = document.getElementById("submitEditProduct");
	var loaderEditProduct = document.getElementById("loaderEditProduct");
	var FormDataWaiting = null;
	
	//les var string qui von sauvegarder les nouveau edit pour les remplacer dans le tableau
	var nname = null;
	var ddescription = null;
	var pprice = null;
	var qqty = null;
	var ccategorie = null;
	var editIsValid = false;
	var tofindid = null; //variable qui va servir a stocker le id du <TR> ou se trouve le produit quon edit
	var requestSelect = null; //variable qui va stocker le select pour la comparer quand on update pour savoir si on delete la ligne ou non
	
	var title = document.getElementById("inttitle");
	
	var productTable = document.getElementById("interfacemanageproducttable");
	
	var he = document.getElementById("hiddenedit");
	var hef = document.getElementById("hiddeneditform");
	
	var arrayToDelete = [];
	
	var deleteButton = document.getElementById("submitDeleteProduct");
	
	deleteButton.onclick = function(e)
	{
		var str = arrayToDelete.toString();
	
		//on remplce les , par des -
		for(var i = 0 ; i <= str.length-1 ; i++)
		{
			if(str[i] == ',')
				str = str.replace(",", "-"); 		
		}
				
		deleteRequest(deleteProduct , "admin" , "type=deleteproduct&delete="+str,loaderDeleteProduct,submitDeleteProduct);
	}
	
	function deleteProduct(sData)
	{
		var obj = null;
		
		if(sData != null)
			 obj = JSON.parse(sData);
		
		if(obj != null)
		{
			if(obj[0] != "")
			{
				for(var i = 0 ; i <= obj.length-1 ; i++)
				{
					var todel = document.getElementById("find"+obj[i]);
					var parent = todel.parentNode;
					parent.removeChild(todel);
				}
			}
		}
	}
	
	submitEditProduct.onclick = function(e)
	{
		e.preventDefault();
		var formData = new FormData();
		formData.append("Id",document.getElementById("productEditId").value);
		formData.append("Name" , productEditName.value);
		formData.append("Description",productEditDescription.value);
		formData.append("Price" , productEditPrice.value);
		formData.append("Categoriename" , productEditCategoriename.value);
		formData.append("Qty" , productEditQty.value);
		formData.append("type" , "editproduct")	
		formData.append("PartErr",null);
		if(typeof document.getElementById("productEditPart").files[0] != "undefined")		
			formData.append("productEditPart", document.getElementById("productEditPart").files[0]);
		
		formDataWaiting = formData;
		
		nname = productEditName.value;
		ddescription = productEditDescription.value;
		pprice = productEditPrice.value;
		qqty = productEditQty.value;
		ccategorie = productEditCategoriename.value;
		
		if(document.getElementById("productEditPart").files[0] != null)
		{
	        var formD = new FormData();
	        formD.append("productId" , document.getElementById("productEditId").value);	  
	        formD.append("type","getproductimgSleep")
	        getImgrequest(getImgData,"admin",formD);
	        
	        document.getElementById("productEditPart").value = "";
		}
		
		partrequest(readData , "admin" , formData , loaderEditProduct,submitEditProduct,"editproduct");
		//request(readData ,"admin" ,"type=editproduct&Name="+productEditName.value+"&Description="+productEditDescription.value+"&Price="+productEditPrice.value+"&Qty="+productEditQty.value+"&Categoriename="+productEditCategoriename.value+"&Id="+productEditId.value,loaderEditProduct,submitEditProduct,"editproduct");
	}
	
	
	submitStartProduct.onclick = function(e)
	{
		arrayToDelete = [];
		title.style.display = "none";
		ccategorie = null;
		requestSelect = productManageCategorie.value;
		
		while (productTable.firstChild) 
		    productTable.removeChild(productTable.firstChild);
		
		var tr = document.createElement("tr");
		
		var tdname = document.createElement("td");
		tdname.className = "bluetd"
		tdname.innerHTML = "Name";
		
		var tdescription = document.createElement("td");
		tdescription.className = "bluetd"
		tdescription.innerHTML = "Description";
		
		var tdprice = document.createElement("td");
		tdprice.className = "bluetd"
		tdprice.innerHTML = "Price";
		
		var tdqty = document.createElement("td");
		tdqty.className = "bluetd"
		tdqty.innerHTML = "Qty";
		
		var tdcatname = document.createElement("td");
		tdcatname.className = "bluetd"
		tdcatname.innerHTML = "Categorie";
		
		var tdedit = document.createElement("td");
		tdedit.className = "bluetd"
		tdedit.innerHTML = "Edit";
		
		var tddelete = document.createElement("td");
		tddelete.className = "bluetd"
		tddelete.innerHTML = "Delete";
								
		tr.appendChild(tdname);
		tr.appendChild(tdescription);
		tr.appendChild(tdprice);
		tr.appendChild(tdqty);
		tr.appendChild(tdcatname);
		tr.appendChild(tdedit);
		tr.appendChild(tddelete);
		productTable.appendChild(tr);	

		request(readData , "admin" , "type=productmanage&Name="+productManageName.value+"&Categoriename="+productManageCategorie.value,loaderManageProduct,submitStartProduct,"productmanage")
		e.preventDefault();
	}
	
	submitAddCategorie.onclick = function(e)
	{
		request(readData,"admin","type=addcategorie&Name="+categorieName.value,loaderAddCategorie,submitAddCategorie,"addcat");
		e.preventDefault()
	}
	
	submitAddProduct.onclick = function(e)
	{
		request(readData,"admin","type=addproduct&Name="+productName.value+"&Description="+productDescription.value+"&Price="+productPrice.value+"&Categoriename="+productCategoriename.value+"&Qty="+productQty.value,loaderAddProduct,submitAddProduct,"addproduct");
		e.preventDefault()
	}
	
	he.onclick = function(e)
	{
		he.style.display = "none";
		hef.style.display = "none";
		document.getElementById("productEditPart").value = "";
		
		for(var i = 0 ; i <= arrayIdToClear.length-1 ; i++)
		{
			var todelete = document.getElementById(arrayIdToClear[i]+"Err");
			if(todelete != null)
			{
					todelete.innerHTML = "";
					todelete.style.display = "none";
			}
		}   
		
		var tochange = document.getElementById("find"+tofind);
		if(requestSelect != ccategorie && requestSelect != "All")
		{
			if(ccategorie != null)
				tochange.style.display = "none";
		}
		else
		{
			if(editIsValid)
			{
				tochange.childNodes[0].innerHTML = nname;
				tochange.childNodes[1].innerHTML = ddescription;
				tochange.childNodes[2].innerHTML = pprice;
				tochange.childNodes[3].innerHTML = qqty;
				tochange.childNodes[4].innerHTML = ccategorie;
				editIsValid = false;
			}
		}	
		
		ccategorie = null;
	}
	
	function insert(name)
	{
		var array = categories.childNodes;
		var textarray = [];
		
		//on recupere tous les noms des categories dans textarray
		for(var i = 0 ; i <= array.length -1 ; i++)
			if(array[i].innerHTML != null)
				textarray.push(array[i].innerHTML); 
		
		//on assosie le min au nom quon vue inserer
		var min = name;
		var temp;
		
		//on parcour notre tableau
		for(var i = 0 ; i <= textarray.length-1 ; i++)
		{
			/*
				si le min est plus petit que textarray[i]
				on place le min a lendroit [i] du tableau et 
				on continue ainsi de suite jusqua pour toute les mots
				jusqua temps quon es trier tous le tableau selon lordre
				lexicographique
			*/
			if(min == wich(min,textarray[i]))
			{
				temp = textarray[i];
				textarray[i] = min;
				min = temp;
			}
		}
		
		/*
			vue que a la fin il va y avoir le dernier mot qui ne pourra pas entrer dans le tableau
			var ses un tableau fixe on reajoute la derniere iteration de min
		*/	
		textarray.push(min);
		
		//on remove toute les option de la list
		while (categories.firstChild)
		{
			categories.removeChild(categories.firstChild);
			pc.removeChild(pc.firstChild);
			productManageCategorie.removeChild(productManageCategorie.firstChild);
			productEditCategoriename.removeChild(productEditCategoriename.firstChild);
		}
		
		//et pour finir on on reaffect cette liste au select
		for(var i = 0 ; i < textarray.length ; i++)
		{
			var option = document.createElement("option");
			option.setAttribute("value",textarray[i]);
			option.innerHTML = textarray[i];
			categories.appendChild(option);
		}
		
		for(var i = 0 ; i < textarray.length ; i++)
		{
			var option = document.createElement("option");
			option.setAttribute("value",textarray[i]);
			option.innerHTML = textarray[i];
			pc.appendChild(option);
		}
		
		for(var i = 0 ; i < textarray.length ; i++)
		{
			var option = document.createElement("option");
			option.setAttribute("value",textarray[i]);
			option.innerHTML = textarray[i];
			productManageCategorie.appendChild(option);
		}
		
		for(var i = 0 ; i < textarray.length ; i++)
		{
			var option = document.createElement("option");
			option.setAttribute("value",textarray[i]);
			option.innerHTML = textarray[i];
			productEditCategoriename.appendChild(option);
		}
	}	
	
	//trouve lequel de 2 mot vien avant dans lordre lexicographique
	function wich(firstword , secondword)
	{	
		var max;
		var min;
		
		//on affect min au mot le moin long et max au mot le plus long
		if(firstword > secondword)
		{
			max = firstword;
			min = secondword
		}
		else
		{
			max = secondword;
			min = firstword;
		}
	
		//son test pour savoir lequel des mot es avant lautre dans lordre lexicographique et on le retourne
		for(var i = 0 ; i <= max.length-1 ; i++)
		{
			if(firstword[i] < secondword[i])
				return firstword;
			else if(firstword[i] > secondword[i])
				return secondword;	
			else
				return min;
		}
	}
	
	var xhr = null;
	var xhrimg = null;
	var xhrdelete = null;
	var xhrdeleteimg = null;

	
	var arrayIdToClear = [];
	
	//fonction a utiliser avec ajax qui va redistribuer les image dans la section edit 
	function getImgData(sData)
	{
		var imagesection = document.getElementById("productImgSection");
		var obj = JSON.parse(sData);
		
		while (imagesection.firstChild) 
		    imagesection.removeChild(imagesection.firstChild);
		
		if(obj != null)
		{
			for(var i = 0 ; i <= obj.length-1 ; i++)
			{
				var div = document.createElement("div");
				div.className = "productImageAdminSection";
				div.setAttribute("id","findpimg"+obj[i].id);
				
				var img = document.createElement("img");
				img.setAttribute("src" , "/millionrama/productimg/"+obj[i].id+"_ipid="+obj[i].fileName);		
				img.className = "productImageAdmin";
				
				var p = document.createElement("p");
				p.className = "deleteP";
				p.innerHTML = "X";
				
				var spinner = document.createElement("img");
				spinner.setAttribute("style","flex:1;max-width:30px;height:30px;display:none;");
				spinner.setAttribute("src","img/ring-alt.gif");
				spinner.setAttribute("id","loaderImg"+obj[i].id);
				
				//on attac un event handler en creant un nouveau scope
		        p.onclick = (function(id,div)
		        {
		            return function() 
		            {
		            	var loadertmp = document.getElementById("loaderImg"+id);
		            	
		            	var deleteImgFormData = new FormData();
		            	deleteImgFormData.append("type","deleteimg");
		            	deleteImgFormData.append("imgid",id);
		            	
		            	deleteImg(deleteSdata , "admin" , deleteImgFormData , loadertmp , div);
		            }
		        })(obj[i].id,div);
				
		        p.appendChild(spinner);
				div.appendChild(img);
				div.appendChild(p);
				imagesection.appendChild(div);
			}
		}
	}
	
	function deleteSdata(sData , containerPimg)
	{
		var obj = null;
		
		if(sData != null)
			obj = JSON.parse(sData);
		
		if(obj != null)
		{		
			if(obj == "true")
			{
				var parent = containerPimg.parentNode;
				parent.removeChild(containerPimg);
			}			
		}
	}
	
	function readData(sData , idExtension)
	{
		for(var i = 0 ; i <= arrayIdToClear.length-1 ; i++)
		{
			var todelete = document.getElementById(arrayIdToClear[i]+"Err");
			if(todelete != null)
			{
					todelete.innerHTML = "";
					todelete.style.display = "none";
			}
		}
		arrayIdToClear.splice(0,arrayIdToClear.length);
		
		if(sData != "" && sData != null)
		{
			var obj = JSON.parse(sData);

			for(var i = 0 ; i <= obj.length-1;i++)
			{
				var err = document.getElementById(obj[i].name+idExtension+"Err");

				if(obj[i].msg != null)
				{
					if(err != null)
					{
						err.innerHTML = obj[i].msg;	
						err.style.display = "block";
						err.className = "err"
					}
				}
				else
				{
					if(obj[i].validMsg != null)
					{
						if(err != null)
						{
							err.innerHTML = obj[i].validMsg;
							err.style.display = "block";
							err.className = "valid";	
							
							if(err.id == "NameaddcatErr") //si ses le formulaire dajotu de categorie qui es valid
							{
								categorieName.value = "";
								insert(obj[i].value); //cette methode insere dans les select de categorie la nouvelle categorie
							}
							else if(err.id == "NameaddproductErr") //si ses le formulaire dajout de produit qui es valid
							{
								productName.value = "";
								productDescription.value = "";
								productPrice.value = "";
								productQty.value = "";
							}
							else if(err.id == "NameeditproductErr")//si ses le formulaire dedit qui es valid
							{
								if(formDataWaiting != null)
								{
									/*
									 * faire une requete qui ira chercher le id de limage le plus grand.
									 * Faire +1. Sa va alors etre ce id le id de limage quon vien dajouter
									 */
									console.log(formDataWaiting.getAll("productEditPart"));
									formDataWaiting = null; // on remet a null le formdata
								}
								
								editIsValid = true;
							}
							
						}
					}
					else if(obj[i].categorie != null)
					{					
						var id = obj[i].id;
						var name = obj[i].name;
						var description = obj[i].description;
						var price = obj[i].price;
						var qty = obj[i].quantity;
						var categorieid = obj[i].categorie.id;
						var catname = obj[i].categorie.name;
						
						var tr = document.createElement("tr");
						tr.setAttribute("id","find"+id);
						
						var tdname = document.createElement("td");
						tdname.innerHTML = name;
						
						var tdescription = document.createElement("td");
						tdescription.innerHTML = description;
						
						var tdprice = document.createElement("td");
						tdprice.innerHTML = price;
						
						var tdqty = document.createElement("td");
						tdqty.innerHTML = qty;
						
						var tdcatname = document.createElement("td");
						tdcatname.innerHTML = catname;
						
						var href = document.createElement("a");

						
						var button = document.createElement("button");
						button.innerHTML = "Edit";
						button.setAttribute("id","edit-"+id)
						
						var tdedit = document.createElement("td");
						
						var checkbox = document.createElement("input");
						checkbox.setAttribute("type","checkbox");
						
						var tddelete = document.createElement("td");
						
						href.appendChild(button);
						tddelete.appendChild(checkbox);
						tdedit.appendChild(href);							
						tr.appendChild(tdname);
						tr.appendChild(tdescription);
						tr.appendChild(tdprice);
						tr.appendChild(tdqty);
						tr.appendChild(tdcatname);
						tr.appendChild(tdedit);
						tr.appendChild(tddelete);
						productTable.appendChild(tr);	
						
						/*
						 * on met un listener sur la checkbox de literation 
						 * qui ajoute on delete le id du produit de lieteration
						 * du tableau qui sera envoyer par requete ajax et qui contient
						 * les id des produit a delete de la bd
						 */
						checkbox.onclick = (function(id)
				        {
				            return function() 
				            {
				            	if(arrayToDelete.includes(id))
				            	{
				            		var position = arrayToDelete.indexOf(id);
				            		arrayToDelete.splice(position,1);
				            	}
				            	else
				            		arrayToDelete.push(id);
				            }
				        })(id);
						
						//on attac un event handler en creant un nouveau scope
				        button.onclick = (function(id,name,description,price,qty,catname)
				        {
				            return function() 
				            {
				               
				               var find = document.getElementById("find"+id).childNodes;
				               
				               
				               productEditName.value = find[0].innerHTML;
				               productEditDescription.value = find[1].innerHTML;
				               productEditPrice.value = find[2].innerHTML;
				               productEditQty.value = find[3].innerHTML;
				               productEditCategoriename.value = find[4].innerHTML;
				               productEditId.value = id;
				               tofind = id; // on enregistre le find+id de la ligne que lon va update si tous se passe bien
				               
				               /*
				                * on envoie la requete ajax qui se chargera
				                *  daller chercher les image de ce produit
				                */
				                var formda = new FormData();
				                formda.append("productId" , id);	  
				                formda.append("type","getproductimg")
				                getImgrequest(getImgData,"admin",formda);
				               
				            }
				        })(id,name,description,price,qty,catname);
					}
				}
				arrayIdToClear.push(obj[i].name+idExtension);
			}
		}
	}