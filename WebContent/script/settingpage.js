/**
 * 
 */
		//formulaire edit setting user preference
		var submitEditUser = document.getElementById("submitEditUser");
		var loaderEditUser = document.getElementById("loaderEditUser");
		var password = document.getElementById("Password");
		var pwc = document.getElementById("PasswordConfirmation");
		var oldpassword = document.getElementById("Oldpassword");
		var email = document.getElementById("Email");		
		var xhredituser = null;
		
		//formulaire dajout de carte de credit
		var submitAddCard = document.getElementById("submitAddCard");
		var loaderAddCard = document.getElementById("loaderAddCard");
		var nameoncard = document.getElementById("NameOnCard");
		var cardnumber = document.getElementById("CardNumber");
		var cardtype = document.getElementById("Cardtype");
		var xhraddcard = null;
		
		submitAddCard.onclick = function(e)
		{
			var form = new FormData();
			form.append("NameOnCard" , nameoncard.value);
			form.append("CardNumber",cardnumber.value);
			form.append("Cardtype",cardtype.value);
			
			requestV2(readDataAddCard , "settingacc" , form , loaderAddCard , submitAddCard , "AddCard",xhraddcard);	
			e.preventDefault();
			console.log("add carddddddddddddddd");
		}
		
		submitEditUser.onclick = function(e)
		{
			console.log("allo");
			var form = new FormData();
			form.append("Oldpassword",oldpassword.value);
			form.append("Password",password.value);
			form.append("PasswordConfirmation",pwc.value);
			form.append("Email",email.value);
			
			requestV2(readDataUser , "setting" , form , loaderEditUser , submitEditUser , "EditUser",xhredituser);		
			e.preventDefault();
		}
		
		var arrayIdToClear = [];
		
		function readDataUser(sData , idExtension)
		{
			console.log(sData);
			for(var i = 0 ; i <= arrayIdToClear.length-1 ; i++)
			{
				var todelete = document.getElementById(arrayIdToClear[i]+"Err");
				if(todelete != null)
					{
						todelete.innerHTML = "";
						todelete.style.display = "none";
					}
			}
			
			arrayIdToClear.splice(0,arrayIdToClear.length);
			
			if(sData != "" && sData != null)
			{
				var obj = JSON.parse(sData);
				
				var valid = true;
				
				for(var i = 0 ; i <= obj.length-1;i++)
				{
					var err = document.getElementById(obj[i].name+idExtension+"Err");
			
					if(obj[i].msg != null)
					{
						valid = false;
						if(err != null)
						{
							err.innerHTML = obj[i].msg;	
							err.style.display = "block";
							err.className = "err";
						}
					}
					else
					{
						if(obj[i].validMsg != null)
						{
							if(err != null)
							{
								err.innerHTML = obj[i].validMsg;
								err.style.display = "block";
								err.className = "valid";
							}
						}
					}
					
					arrayIdToClear.push(obj[i].name+idExtension);
				}
				
				if(valid)
				{
					password.value = "";
					pwc.value = "";
					oldpassword.value = "";
				}
			}
		}
		
		function readDataAddCard(sData , idExtension)
		{
			console.log(sData);
			for(var i = 0 ; i <= arrayIdToClear.length-1 ; i++)
			{
				var todelete = document.getElementById(arrayIdToClear[i]+"Err");
				if(todelete != null)
					{
						todelete.innerHTML = "";
						todelete.style.display = "none";
					}
			}
			
			arrayIdToClear.splice(0,arrayIdToClear.length);
			
			if(sData != "" && sData != null)
			{
				var obj = JSON.parse(sData);
				
				var valid = true;
				
				for(var i = 0 ; i <= obj.length-1;i++)
				{
					var err = document.getElementById(obj[i].name+idExtension+"Err");
			
					if(obj[i].msg != null)
					{
						valid = false;
						if(err != null)
						{
							err.innerHTML = obj[i].msg;	
							err.style.display = "block";
							err.className = "err";
						}
					}
					else
					{
						if(obj[i].validMsg != null)
						{
							if(err != null)
							{
								err.innerHTML = obj[i].validMsg;
								err.style.display = "block";
								err.className = "valid";
							}
						}
					}
					
					arrayIdToClear.push(obj[i].name+idExtension);
				}
				
				if(valid)
				{
					nameoncard.value = "";
					cardnumber.value = "";
					
					
					var cc = document.getElementById("ccmanage");
					
					if(cc.style.display == "none")
						cc.style.display = "block";
					
					var tr = document.createElement("tr");
					
					var tdtype = document.createElement("td");
					tdtype.innerHTML = obj[2].value;
					tdtype.setAttribute("style","background-color:white");
					
					var tdname = document.createElement("td");
					tdname.innerHTML = obj[0].value;
					tdname.setAttribute("style","background-color:white");
					
					var tdnumber = document.createElement("td");
					tdnumber.innerHTML = obj[1].value;
					tdnumber.setAttribute("style","background-color:white");
					
					tr.appendChild(tdtype);
					tr.appendChild(tdname);
					tr.appendChild(tdnumber);
					cc.appendChild(tr);
				}
			}
		}