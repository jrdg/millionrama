package com.tp.forms;

import com.tp.exceptions.BeanException;
import com.tp.exceptions.JGErrorException;
import com.tp.exceptions.JGValidException;

public class RegisterForm 
{
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private String passwordConfirmation;
	private String g_recaptcha_response;

	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) throws JGErrorException , JGValidException
	{
		if(firstname.isEmpty() || firstname == null)
			throw new JGErrorException("firstname cannot be empty");
		else if(firstname.length() > 20 || firstname.length() < 2)
			throw new JGErrorException("Firstname need more than 1 and less than 20 character");
		else		
			this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) throws JGErrorException {
		
		if(lastname.isEmpty() || lastname == null)
			throw new JGErrorException("Lastname cannot be empty");
		else if(lastname.length() > 20 || lastname.length() < 2)
			throw new JGErrorException("Lastname need more than 1 and less than 20 character");
		else		
			this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) throws JGErrorException {
		if(email.isEmpty() || email == null)
			throw new JGErrorException("Email cannot be empty");
		else		
			this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) throws JGErrorException {
		
		if(password.isEmpty() || password == null)
			throw new JGErrorException("Password cannot be empty");
		else
			this.password = password;
	}
	
	public String getPasswordConfirmation() {		
		return passwordConfirmation;
	}
	public void setPasswordConfirmation(String passwordConfirmation)  throws JGErrorException {
			this.passwordConfirmation = passwordConfirmation;
			
			if(this.password == null || this.password.isEmpty() || !this.password.equals(this.passwordConfirmation))
				throw new JGErrorException("Password confirmation need to be the same as the password");				
	}
	
	public String getG_recaptcha_response() {
		return g_recaptcha_response;
	}
	public void setG_recaptcha_response(String g_recaptcha_response) throws JGErrorException {
		
		if(g_recaptcha_response.isEmpty() || g_recaptcha_response == null)
			throw new JGErrorException("Invalid capcha");
		else
			this.g_recaptcha_response = g_recaptcha_response;
	}
}
