package com.tp.forms;

import com.tp.exceptions.JGErrorException;

public class ManageProductForm 
{
	private String name;
	private String categoriename;
	
	public String getName() {
		return name;
	}
	public void setName(String name) throws JGErrorException {
		this.name = name;
	}
	public String getCategoriename() {
		return categoriename;
	}
	public void setCategoriename(String categoriename) {
		this.categoriename = categoriename;
	}
	
	
}
