package com.tp.forms;

import com.tp.exceptions.BeanException;
import com.tp.exceptions.JGErrorException;
import com.tp.exceptions.JGValidException;

public class AddCategorieForm 
{
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) throws JGErrorException , JGValidException {
		if(name == null || name.isEmpty())
			throw new JGErrorException("You need to fill the categorie name");
		else if(name.length() < 2 || name.length() > 20)
			throw new JGErrorException("Categorie name need at least 2 character and a maximum of 20");
		else if(!Character.isUpperCase(name.charAt(0)))
			throw new JGErrorException("The first letter must be an upper case");
		else
		{
			this.name = name;
			throw new JGValidException(this.name+" have been added to the categorie");
		}
	}	
}
