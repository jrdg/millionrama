package com.tp.forms;

import com.tp.beans.Categorie;
import com.tp.exceptions.JGErrorException;
import com.tp.exceptions.JGValidException;
import com.tp.utils.Utils;

public class AddProductAdminForm 
{
	private String name;
	private String description;
	private String price;
	private String categoriename;
	private String qty;
	
	public String getName() {
		return name;
	}
	public void setName(String name) throws JGErrorException , JGValidException {
		if(name.isEmpty() || name == null)
			throw new JGErrorException("Product name cannot be null");
		else if(name.length() < 2 || name.length() > 20)
			throw new JGErrorException("Name can contain between 2 and 20 character");
		else
			this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description)  throws JGErrorException{
		if(description.isEmpty() || description == null)
			throw new JGErrorException("Product description cannot be null");
		else if(description.length() < 10 || description.length() > 255)
			throw new JGErrorException("Description can contain between 10 and 255 character");
		else
			this.description = description;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price)  throws JGErrorException{
		if(price.isEmpty() || price == null)
			throw new JGErrorException("Product need a price");
		else if(!Utils.isDouble(price))
			throw new JGErrorException("Price need to be a number");
		else if(Double.parseDouble(price) < 1000000)
			throw new JGErrorException("Price need to be at least 1million$");
		else
			this.price = price;
	}
	public String getCategoriename() {
		return categoriename;
	}
	public void setCategoriename(String categoriename) {
		this.categoriename = categoriename;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) throws JGErrorException{
		if(qty.isEmpty() || qty == null)
			throw new JGErrorException("Product need a quantity");
		else if(!Utils.isInt(qty))
			throw new JGErrorException("Quantity need to be a number");
		else
			this.qty = qty;
	}
	
	
	
}
