package com.tp.forms;

import com.tp.exceptions.BeanException;

public class LoginForm 
{
	private String email;
	private String password;
	
	public String getEmail() throws BeanException {	
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
