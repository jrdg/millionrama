package com.tp.forms;

import com.tp.exceptions.JGErrorException;
import com.tp.utils.Utils;

public class PayWithExisting
{
	private String creditCardId;
	private String cvv;
	private String month;
	private String year;
	
	public String getCreditCardId() {
		return creditCardId;
	}
	public void setCreditCardId(String creditCardId) {
		this.creditCardId = creditCardId;
	}
	
	public String getCvv() {
		return cvv;
	}
	
	public void setCvv(String cvv) throws JGErrorException {
		if(cvv.isEmpty() || cvv == null)
			throw new JGErrorException("Cvv cant be null");
		else if(cvv.length() != 3)
			throw new JGErrorException("Cvv need 3 number");
		else if(!Utils.isInt(cvv))
			throw new JGErrorException("cvv need to be a number");
		else
		this.cvv = cvv;
	}
	
	public String getMonth() {
		return month;
	}
	
	public void setMonth(String month) throws JGErrorException {
		this.month = month;
	}
	
	public String getYear() {
		return year;
	}
	
	public void setYear(String year) throws JGErrorException {
			this.year = year;
	}
	
	

}
