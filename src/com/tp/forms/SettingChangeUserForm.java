package com.tp.forms;

import com.tp.exceptions.JGErrorException;

public class SettingChangeUserForm 
{
	private String oldpassword;
	private String password;
	private String passwordConfirmation;
	private String email;
	
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) throws JGErrorException {
		if(password.isEmpty() || password == null)
			throw new JGErrorException("Password cannot be empty");
		else
			this.password = password;
	}
	
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}
	
	public void setPasswordConfirmation(String passwordConfirmation) throws JGErrorException {
		this.passwordConfirmation = passwordConfirmation;		
		if(password == null || password.isEmpty() || !password.equals(this.passwordConfirmation))
			throw new JGErrorException("Password confirmation need to be the same as the password");	
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) throws JGErrorException {	
		if(email.isEmpty() || email == null)
			throw new JGErrorException("Email cannot be empty");
		else		
			this.email = email;
	}

	public String getOldpassword() {
		return oldpassword;
	}

	public void setOldpassword(String oldpassword) throws JGErrorException {
		if(oldpassword.isEmpty() || oldpassword == null)
			throw new JGErrorException("Old password cannot be empty");
		else
			this.oldpassword = oldpassword;
	}
	
	
}
