package com.tp.forms;

import com.tp.exceptions.JGErrorException;

public class SettingAddCreditCardForm 
{
	private String cardtype;
	private String nameOnCard;
	private String cardNumber;
	private int month;
	private int years;
	
	
	
	public String getCardtype() {	
		return cardtype;
	}
	
	public void setCardtype(String cardtype) throws JGErrorException {
		if(cardtype.isEmpty() || cardtype == null)
			throw new JGErrorException("You must select a card type");
		else if(!(cardtype.equals("Mastercard") || cardtype.equals("Visa") || cardtype.equals("American express")))
			throw new JGErrorException("You must a select a valid card type");
		else
			this.cardtype = cardtype;
	}
	
	public String getNameOnCard() {
		return nameOnCard;
	}
	
	public void setNameOnCard(String nameOnCard) throws JGErrorException {
		
		if(nameOnCard.isEmpty() || nameOnCard == null)
			throw new JGErrorException("You must have a name");
	}
	
	public String getCardNumber() {
		return cardNumber;
	}
	
	public void setCardNumber(String cardNumber) throws JGErrorException {
		
		if(cardNumber.isEmpty() || cardNumber == null)
			throw new JGErrorException("Card number cannot be null");
		else if(cardNumber.length() != 16)
			throw new JGErrorException("Invalid card number you need 16 digit");
		else
			this.cardNumber = cardNumber;
	}
	
	public int getMonth() {
		return month;
	}
	
	public void setMonth(int month) {
		this.month = month;
	}
	
	public int getYears() {
		return years;
	}
	
	public void setYears(int years) {
		this.years = years;
	}
	
	
}
