package com.tp.beans;

public class GuidUser
{
	private int id;
	private int userId;
	private String guid;
	private User user;
	
	public GuidUser(int userId , String guid)
	{
		this.userId = userId;
		this.guid = guid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
}
