package com.tp.beans;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Panier 
{
	private HashMap panier;
	
	public Panier()
	{
		panier = new HashMap();
	}
	
	public void Add(Produit p)
	{
		panier.put(p.getId(), p);
	}
	
	public void remove(int id)
	{
		panier.remove(id);
	}
	
	public boolean AlreadyIn(int id)
	{
		if(panier.get(id) != null)
			return true;
		
		return false;
	}
	
	public double calculateGlobalPrice()
	{
		Iterator list = panier.entrySet().iterator();
		double price = 0;
		
		while(list.hasNext())
		{
			Map.Entry pair = (Map.Entry)list.next();
			Produit p = (Produit) pair.getValue();			
			price += p.getQuantity() * p.getPrice();
		}
		
		return price;
	}
	
	public void updatePrice(int key , double price)
	{
		((Produit)panier.get(key)).setPrice(price);
	}
	
	public static void iterateToString(HashMap hm)
	{
		Iterator list = hm.entrySet().iterator();
		
		while(list.hasNext())
		{
			Map.Entry pair = (Map.Entry)list.next();
			Produit p = (Produit) pair.getValue();			
			System.out.println(p.getName() + " qty :" + p.getQuantity());
		}
	}

	public HashMap getPanier() {
		return panier;
	}

	public void setPanier(HashMap panier) {
		this.panier = panier;
	}
}
