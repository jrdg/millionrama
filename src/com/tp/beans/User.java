package com.tp.beans;

import com.tp.exceptions.BeanException;
import com.tp.interfaceDAO.UsersDAO;

public class User
{
	private int id;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private String passwordConfirmation;
	private boolean status;
	private int roleId;
	private Role role;
	
	public User(String firstname , String lastname , String email , String password)
	{
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
	}

	public User() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname)
	{	
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	
	public void setLastname(String lastname){		
		this.lastname = lastname;
	}
	public String getEmail(){
		return email;
	}
	public void setEmail(String email) {	
			this.email = email;
	}
	public String getPassword(){
		return password;
	}
	public void setPassword(String password){
		this.password = password;
	}
	public int getRoleId(){
		return roleId;
	}
	public void setRoleId(int roleId){
		this.roleId = roleId;
	}
	public Role getRole(){
		return role;
	}
	public void setRole(Role role){
		this.role = role;
	}
	
	public String getPasswordConfirmation(){		
		return passwordConfirmation;
	}
	public void setPasswordConfirmation(String passwordConfirmation){
			this.passwordConfirmation = passwordConfirmation;				
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}