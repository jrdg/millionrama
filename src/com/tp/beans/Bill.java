package com.tp.beans;

import java.util.ArrayList;

public class Bill
{
	private int id;
	private int userid;
	private ArrayList<Produit> plist;
	private double total;
	
	public Bill()
	{
		this.total = 0;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public ArrayList<Produit> getPlist() {
		return plist;
	}
	public void setPlist(ArrayList<Produit> plist) {
		this.plist = plist;
	}
	
	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public void calculeTotal()
	{
		for (Produit produit : plist) {
			double more = produit.getQuantity() * produit.getPrice();
			this.total += more;
		}
	}
	
	
}
