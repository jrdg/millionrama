package com.tp.beans;

public class Creditcard
{
	private int id;
	private String type;
	private String name;
	private String cardnumber;
	private int userid;
	
	public Creditcard()
	{
		this.type = null;
		this.name = null;
		this.cardnumber = null;
	}
	
	public Creditcard(String type , String name , String cardnumber , int userid)
	{
		this.type = type;
		this.name = name;
		this.cardnumber = cardnumber;
		this.userid = userid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
