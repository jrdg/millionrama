package com.tp.beans;

public class Image 
{
	private int id;
	private int produitId;
	private String fileName;
	private String path;
	
	public Image(String fileName , String path)
	{
		this.fileName = fileName;
		this.path = path;
	}
	
	public Image(int id , String fileName , String path)
	{
		this.produitId = id;
		this.fileName = fileName;
		this.path = path;
	}
	
	public Image(int id , int produitid , String filename , String path)
	{
		this.id = id;
		this.produitId = produitid;
		this.fileName = filename;
		this.path = path;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProduitId() {
		return produitId;
	}

	public void setProduitId(int produitId) {
		this.produitId = produitId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}