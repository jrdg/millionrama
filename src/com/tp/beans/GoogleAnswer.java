package com.tp.beans;

public class GoogleAnswer 
{
	private boolean success;
	private String challenge_ts;
	private String hostname;
	
	public GoogleAnswer()
	{
		
	}

	public boolean getSucces() {
		return success;
	}

	public void setSucces(boolean succes) {
		this.success = succes;
	}

	public String getChallenge_ts() {
		return challenge_ts;
	}

	public void setChallenge_ts(String challenge_ts) {
		this.challenge_ts = challenge_ts;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	
	
}
