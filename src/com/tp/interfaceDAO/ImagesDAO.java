package com.tp.interfaceDAO;

import java.util.ArrayList;

import com.tp.beans.Image;
import com.tp.exceptions.DaoException;

public interface ImagesDAO 
{
	int Add(Image img) throws DaoException;	
	ArrayList<Image> getAllByProductId(int productId);
	void Delete(int id) throws DaoException;
	boolean idExist(int id);
	ArrayList<Image> getProductImg(int id);
}
