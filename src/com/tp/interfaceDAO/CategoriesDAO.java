package com.tp.interfaceDAO;

import java.util.ArrayList;

import com.tp.beans.Categorie;
import com.tp.exceptions.DaoException;

public interface CategoriesDAO 
{
	ArrayList<Categorie> getAll();
	int Add(Categorie categorie) throws DaoException;
	boolean doNameIstaken(String name);
	int getIdByName(String name);
	Categorie getCategorieById(int id);
}
