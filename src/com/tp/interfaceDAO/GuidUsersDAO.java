package com.tp.interfaceDAO;

import com.tp.beans.GuidUser;
import com.tp.exceptions.DaoException;

public interface GuidUsersDAO 
{
	int add(GuidUser guiduser) throws DaoException; //ajoute un guid lors de linscription
	GuidUser getGuid(int userid , String guid) throws DaoException; //retourne un guid contenant les info du guid et du user 
	void desactive(int id) throws DaoException; //desactive un guid quand lutilisateur active son compte
	GuidUser getguid(int id);
}
