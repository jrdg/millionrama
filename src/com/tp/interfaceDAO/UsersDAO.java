package com.tp.interfaceDAO;


import java.util.ArrayList;

import com.tp.beans.User;
import com.tp.exceptions.DaoException;

public interface UsersDAO
{
	int Add(User user) throws DaoException;
	void Activate(int userid) throws DaoException;
	int MatchEmailPassword(String email , String password);
	boolean isActif(int userid);
	void Delete(int id);
	void Update(User user) throws DaoException;
	boolean doEmailIsTaken(String email); 
	ArrayList<User> GetAll();
	User getUserByEmail(String email);
}
