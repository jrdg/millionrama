package com.tp.interfaceDAO;

import java.util.ArrayList;

import com.tp.beans.Produit;
import com.tp.exceptions.DaoException;

public interface ProductsDAO 
{
    ArrayList<Produit> getAllProducts();   
	int Add(Produit product) throws DaoException;
	ArrayList<Produit> getProductByNameAndOrCategorie(int categorieid, String productname);
	void Edit(Produit product) throws DaoException;
	void Delete(int id) throws DaoException;
	Produit getProduct(int id);
	ArrayList<Produit> getProductByNameAndOrCategorie2(int categorieid, String productname);
	ArrayList<Produit> getLast5();
}
