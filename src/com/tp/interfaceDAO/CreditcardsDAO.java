package com.tp.interfaceDAO;

import java.util.ArrayList;

import com.tp.beans.Creditcard;
import com.tp.exceptions.DaoException;

public interface CreditcardsDAO {
	int Add(Creditcard cc) throws DaoException;
	ArrayList<Creditcard> getAll(int userid);
	boolean valideCardUser(int userid , int cardid);
}
