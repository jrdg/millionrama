package com.tp.DAO;

import com.tp.beans.Bill;
import com.tp.beans.Produit;
import com.tp.exceptions.DaoException;

public interface BillsDAO {
	int Add(Bill bill) throws DaoException;
	int AddProductToBill(Produit p , int billId) throws DaoException;
	Bill getBillById(int billId);
}
