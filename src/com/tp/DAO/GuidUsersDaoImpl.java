package com.tp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.tp.beans.GuidUser;
import com.tp.beans.User;
import com.tp.exceptions.DaoException;
import com.tp.interfaceDAO.GuidUsersDAO;

public class GuidUsersDaoImpl implements GuidUsersDAO
{
	private DaoFactory df;

	public GuidUsersDaoImpl(DaoFactory daoFactory)
	{
		this.df = daoFactory;
	}
	
	@Override
	public int add(GuidUser guiduser) throws DaoException
	{
		Connection connexion = null;
		try {
			connexion = df.getConnection();
			
			PreparedStatement ps = connexion.prepareStatement("insert into guid(userid , guid) values(?,?)",PreparedStatement.RETURN_GENERATED_KEYS);
			
			ps.setInt(1, guiduser.getUserId());
			ps.setString(2, guiduser.getGuid());
			
			ps.executeUpdate();
			connexion.commit();
			
			ResultSet result = ps.getGeneratedKeys();
			
			if(result.next())
				return result.getInt(1);
			else
				return -1;
			
		}
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
	}

	//retourne un guid avec un user es utiliser dans la page activation de compte
	@Override
	public GuidUser getGuid(int userid , String guid) throws DaoException {
		Connection connexion = null;
		try {
		    connexion = this.df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("SELECT * FROM guid inner JOIN users ON guid.userid = users.id where guid.userid = ? && guid.status = ? && guid.guid = ?");
			
			ps.setInt(1, userid);
			ps.setBoolean(2, true);
			ps.setString(3, guid);
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
			{
				User user = new User();
				user.setFirstname(result.getString("users.firstname"));
				user.setLastname(result.getString("users.lastname"));
				user.setEmail((result.getString("users.email")));	
				user.setId(result.getInt("users.id"));
				GuidUser gu = new GuidUser(result.getInt("guid.userid"),result.getString("guid.guid"));
				gu.setId(result.getInt("guid.id"));
				gu.setUser(user);
				return gu;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			try {
				if(connexion != null)
					connexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		return null;
	}

	@Override
	public void desactive(int guidId) throws DaoException {
		Connection connexion = null;
		try 
		{
			connexion = this.df.getConnection();
			
			PreparedStatement ps = connexion.prepareStatement("UPDATE guid SET status = 0 where id = ?");
			
			ps.setInt(1, guidId);
			
			ps.executeUpdate();
			
			connexion.commit();
		} 
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
		
	}

	@Override
	public GuidUser getguid(int id) {
		Connection connexion = null;
		
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select * from guid where id = ?");
			
			ps.setInt(1, id);
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
				return new GuidUser(result.getInt("userid"),result.getString("guid"));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
				try {
					if(connexion != null)
						connexion.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		return null;
	}

}
