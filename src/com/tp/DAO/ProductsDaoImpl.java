package com.tp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.tp.beans.Categorie;
import com.tp.beans.Image;
import com.tp.beans.Produit;
import com.tp.exceptions.DaoException;
import com.tp.interfaceDAO.ProductsDAO;

public class ProductsDaoImpl implements ProductsDAO
{
	private DaoFactory df;
	
	public ProductsDaoImpl(DaoFactory df)
	{
		this.df = df;
	}

	@Override
	public int Add(Produit product) throws DaoException {
		Connection connexion = null;
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement( "insert into produits(name,description,prix,qty,categorieid) values(? , ? , ? , ? , ?)" , PreparedStatement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, product.getName());
			ps.setString(2, product.getDescription());
			ps.setDouble(3, product.getPrice());
			ps.setInt(4, product.getQuantity());
			ps.setInt(5, product.getCategorieId());
			
			ps.executeUpdate();		
			connexion.commit();
			
			ResultSet result = ps.getGeneratedKeys();
			
			if(result.next())
				return result.getInt(1);
			
		} 
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
		
		return -1;
	}
	
	//retourne un arrayilist de produit selon un search like statement et selon une categorie ou non
	@Override
	public ArrayList<Produit> getProductByNameAndOrCategorie2(int categorieid, String productname) {	

		PreparedStatement ps = null;
		ArrayList<Produit> list = new ArrayList<Produit>();
		Connection connexion = null;
		try {
			
			connexion = df.getConnection();
			
			if(productname != null && categorieid > 0)
			{
				ps = connexion.prepareStatement("select * from produits inner join categories on produits.categorieid = categories.id where categorieid = ? && produits.name like ? order by produits.name");
				ps.setInt(1 , categorieid);
				ps.setString(2 , "%"+productname+"%");
			}
			else if(categorieid == 0 && productname != null)
			{
				ps = connexion.prepareStatement("select * from produits inner join categories on produits.categorieid = categories.id where produits.name like ? order by produits.name");
				ps.setString(1 , "%"+productname+"%");
			}
			else
			{
				ps = connexion.prepareStatement("select * from produits inner join categories on produits.categorieid = categories.id where categories.id = ? order by produits.name");
				ps.setInt(1 , categorieid);
			}
			
			ResultSet result = ps.executeQuery();
			
			while(result.next())
			{
				Produit produit = new Produit();
				produit.setName(result.getString("produits.name"));
				produit.setDescription(result.getString("produits.description"));
				produit.setId(result.getInt("produits.id"));
				produit.setPrice(result.getDouble("produits.prix"));
				produit.setQuantity(result.getInt("produits.qty"));	
				Categorie cat = new Categorie(result.getString("categories.name"));
				cat.setId(result.getInt("categories.id"));
				produit.setCategorie(cat);
				
				list.add(produit);
			}
			
			return list;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		finally{
			try
			{
				if(connexion != null)
					connexion.close();
			}
			catch(SQLException e){
				
			}
		}
		
		return null;
	}

	//retourne un arrayilist de produit selon un search like statement et selon une categorie ou non
	@Override
	public ArrayList<Produit> getProductByNameAndOrCategorie(int categorieid, String productname) {	

		PreparedStatement ps = null;
		ArrayList<Produit> list = new ArrayList<Produit>();
		Connection connexion = null;
		try {
			
			connexion = df.getConnection();
			
			if(productname != null && categorieid > 0)
			{
				ps = connexion.prepareStatement("select * from produits inner join categories on produits.categorieid = categories.id where categorieid = ? && produits.name like ? order by produits.name");
				ps.setInt(1 , categorieid);
				ps.setString(2 , "%"+productname+"%");
			}
			else if(categorieid == -1 && productname != null)
			{
				ps = connexion.prepareStatement("select * from produits inner join categories on produits.categorieid = categories.id where produits.name like ? order by produits.name");
				ps.setString(1 , "%"+productname+"%");
			}
			else
			{
				ps = connexion.prepareStatement("select * from produits inner join categories on produits.categorieid = categories.id where categories.id = ? order by produits.name");
				ps.setInt(1 , categorieid);
			}
			
			ResultSet result = ps.executeQuery();
			
			while(result.next())
			{
				Produit produit = new Produit();
				produit.setName(result.getString("produits.name"));
				produit.setDescription(result.getString("produits.description"));
				produit.setId(result.getInt("produits.id"));
				produit.setPrice(result.getDouble("produits.prix"));
				produit.setQuantity(result.getInt("produits.qty"));	
				Categorie cat = new Categorie(result.getString("categories.name"));
				cat.setId(result.getInt("categories.id"));
				produit.setCategorie(cat);
				
				list.add(produit);
			}
			
			return list;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		finally{
			try
			{
				if(connexion != null)
					connexion.close();
			}
			catch(SQLException e){
				
			}
		}
		
		return null;
	}

	@Override
	public void Edit(Produit p) throws DaoException {
		Connection connexion = null;
		try {
			
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("UPDATE produits SET name = ?, description = ? , prix = ? , qty = ? , categorieid = ? WHERE id = ?");
			
			ps.setString(1, p.getName());
			ps.setString(2, p.getDescription());
			ps.setDouble(3, p.getPrice());
			ps.setInt(4, p.getQuantity());
			ps.setInt(5, p.getCategorieId());
			ps.setInt(6, p.getId());
			
			ps.executeUpdate();
			connexion.commit();

		}
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
	}

	@Override
	public void Delete(int id) throws DaoException {
		Connection connexion = null;
		try {
			
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("delete from produits where id = ?");
			
			ps.setInt(1, id);
			
			ps.executeUpdate();
			connexion.commit();
			
		} 
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }	
	}

    @Override
    public  ArrayList<Produit> getAllProducts() {
    	
    	Connection connexion = null;
        ArrayList<Produit> retour = null;
        String queryEtudiantAll = "select * from produits";
		try {
            connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement(queryEtudiantAll);

			ResultSet result = ps.executeQuery();

			if (result.isBeforeFirst())
				retour = new ArrayList<>();

			while (result.next()) {
				Produit product = new Produit();
				product.setId(result.getInt("id"));
                                product.setPrice(result.getDouble("prix"));
				product.setName(result.getString("name"));
				product.setDescription(result.getString("description"));
				
				
				retour.add(product);
			}
			// Atttttteeeeeeeeennnnnnnnnttttttttiiiiiiiiiooooooooonnnnnnnn
			// ne pas oublieeeeeeeeeeeeeeeeeeeeeeer
			// ********************
			connexion.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return retour;
	}

	@Override
	public Produit getProduct(int id) {
		Connection connexion = null;
		
		
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select * from produits where id =  ?");

			ps.setInt(1, id);
			
			ResultSet result = ps.executeQuery();
					
			if(result.next())
			{
				Produit p = new Produit();
				p.setCategorieId(result.getInt("categorieid"));
				p.setDescription(result.getString("description"));
				p.setId(result.getInt("id"));
				p.setName(result.getString("name"));
				p.setPrice(result.getDouble("prix"));
				p.setQuantity(result.getInt("qty"));
				
				return p;
			}
	
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			try {
				if(connexion != null)
					connexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		return null;
	}

	@Override
	public ArrayList<Produit> getLast5() {
		
		Connection connexion = null;
		ArrayList<Produit> list = new ArrayList<Produit>();
		
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select * from produits  order by id desc limit 5");
			
			ResultSet result = ps.executeQuery();
			
			while(result.next())
			{
				Produit p = new Produit();
				p.setCategorieId(result.getInt("categorieid"));
				p.setDescription(result.getString("description"));
				p.setId(result.getInt("id"));
				p.setName(result.getString("name"));
				p.setPrice(result.getDouble("prix"));
				p.setQuantity(result.getInt("qty"));
				
				list.add(p);
			}
			
			return list;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
