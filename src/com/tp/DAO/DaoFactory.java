package com.tp.DAO;

import java.security.MessageDigest;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.tp.beans.Role;
import com.tp.beans.User;

public class DaoFactory
{
    private String url;
    private String username;
    private String password;

    DaoFactory(String url, String username, String password) 
    {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    //a utiliser dans la servlet quand on linstancie
    public static DaoFactory getInstance()
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {

        }

        DaoFactory instance = new DaoFactory("xxxxxx", "xxxxxx", "xxxxx");
        return instance;
    }

    //a utiliser dans les requetes
    public Connection getConnection() throws SQLException
    {
        Connection connexion = DriverManager.getConnection(url, username, password);
        connexion.setAutoCommit(false);
        return connexion;
    }

    // R�cup�ration du Dao
    public UsersDaoImpl getUsersDao() 
    {
        return new UsersDaoImpl(this);
    }
    
    public GuidUsersDaoImpl getGuidDao()
    {
    	return new GuidUsersDaoImpl(this);
    }
    
    public CategoriesDaoImpl getCategoriesDao()
    {
    	return new CategoriesDaoImpl(this);
    }
    
    public ProductsDaoImpl getProductsDao()
    {
    	return new ProductsDaoImpl(this);
    }
    
    public ImagesDaoImpl getImageDao()
    {
    	return new ImagesDaoImpl(this);
    }
    
    public CreditcardDaoImpl getCreditcardsDao()
    {
    	return new CreditcardDaoImpl(this);
    }
    
    public BillsDaoImpl getBillsDao()
    {
    	return new BillsDaoImpl(this);
    }
}