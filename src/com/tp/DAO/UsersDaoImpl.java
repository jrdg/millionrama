package com.tp.DAO;

import java.sql.*;
import java.util.ArrayList;

import com.tp.beans.Role;
import com.tp.beans.User;
import com.tp.exceptions.DaoException;
import com.tp.interfaceDAO.UsersDAO;
import com.tp.utils.Utils;

public class UsersDaoImpl implements UsersDAO
{
	private DaoFactory df;

	public UsersDaoImpl(DaoFactory daoFactory)
	{
		this.df = daoFactory;
	}

	@Override
	public int Add(User user) throws DaoException {
		
		Connection connexion = null;
		
		try{
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("insert into users(firstname,lastname,email,password,roleid,status) values(?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, user.getFirstname());
			ps.setString(2, user.getLastname());
			ps.setString(3, user.getEmail());
			ps.setString(4, Utils.hashMD5(user.getPassword()));
			ps.setInt(5, user.getRoleId());
			ps.setBoolean(6, user.isStatus());
			
			ps.executeUpdate();
			connexion.commit();
			
			ResultSet result = ps.getGeneratedKeys();
			
			if(result.next())
				return result.getInt(1);
			else
				return -1;
					
		}
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
	}

	@Override
	public void Delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Update(User user) throws DaoException {
		
		Connection connexion = null;
		
		try
		{
			connexion = df.getConnection();
			
			String query = null;
			
			if(user.getPassword().isEmpty() || user.getPassword() == null)
				query = "UPDATE users SET email = ? where id = ?";
			else
				query = "UPDATE users SET email = ? , password = ? where id = ?";
			
			PreparedStatement ps = connexion.prepareStatement(query);

			ps.setString(1, user.getEmail());
			
			if(user.getPassword().isEmpty() || user.getPassword() == null)
				ps.setInt(2, user.getId());
			else
			{
				ps.setString(2, Utils.hashMD5(user.getPassword()));
				ps.setInt(3, user.getId());
			}
			
			ps.executeUpdate();
			connexion.commit();
			
			
			
		} 
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
		
	}

	@Override
	public ArrayList<User> GetAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean doEmailIsTaken(String email) {
		Connection connexion = null;
		try {
			connexion = df.getConnection();
			
			PreparedStatement ps = connexion.prepareStatement("select email from users where email = ?");
			
			ps.setString(1, email);	
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
				return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(connexion != null)
					connexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		return false;
	}

	@Override
	public void Activate(int userid) throws DaoException 
	{
		Connection connexion = null;
		try 
		{
			connexion = df.getConnection();
			
			PreparedStatement ps = connexion.prepareStatement("UPDATE users SET status = 1 where id = ?");
			
			ps.setInt(1, userid);
			
			ps.executeUpdate();
			
			connexion.commit();
		} 
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
	}

	@Override
	public int MatchEmailPassword(String email, String password) {
		Connection connexion = null;
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select id, email , password from users where email = ? && password = ?");
			
			ps.setString(1, email);
			ps.setString(2, Utils.hashMD5(password));
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
				return result.getInt("id");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(connexion != null)
					connexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		return -1;
	}

	@Override
	public boolean isActif(int userid) {
		Connection connexion = null;
		try 
		{
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select status from users where id = ?");
			
			ps.setInt(1, userid);
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
			{
				if(result.getBoolean("status"))
					return true;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(connexion != null)
					connexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return false;
	}

	@Override
	public User getUserByEmail(String email) {
		Connection connexion = null;
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select * from users inner join roles on users.roleid = roles.id where users.email = ?");
			
			ps.setString(1, email);
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
			{
				Role role = new Role();
				
				role.setId(result.getInt("roles.id"));
				role.setName(result.getString("roles.name"));
				
				User user = new User();
				user.setEmail(result.getString("users.email"));
				user.setFirstname(result.getString("users.firstname"));
				user.setLastname(result.getString("users.lastname"));
				user.setId(result.getInt("users.id"));
				user.setPassword(result.getString("users.password"));
				user.setRoleId(result.getInt("users.id"));
				user.setStatus(result.getBoolean("users.status"));
				user.setRole(role);
				
				return user;
			}	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(connexion != null)
					connexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		return null;
	}

}