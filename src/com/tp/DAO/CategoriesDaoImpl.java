package com.tp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.tp.beans.Categorie;
import com.tp.exceptions.DaoException;
import com.tp.interfaceDAO.CategoriesDAO;

public class CategoriesDaoImpl implements CategoriesDAO
{
	private DaoFactory df;
	
	public CategoriesDaoImpl(DaoFactory df)
	{
		this.df = df;
	}

	@Override
	public ArrayList<Categorie> getAll() 
	{
		ArrayList<Categorie> list = new ArrayList<Categorie>();
		Connection connexion = null;
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select * from categories order by name");
			
			ResultSet result = ps.executeQuery();
			
			while(result.next())
				list.add(new Categorie(result.getInt("id"),result.getString("name")));
			return list;	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(connexion != null)
					connexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
	}

	@Override
	public int Add(Categorie categorie) throws DaoException {
		Connection connexion = null;
			try {
				connexion = df.getConnection();
				PreparedStatement ps = connexion.prepareStatement("insert into categories(name) values(?)", PreparedStatement.RETURN_GENERATED_KEYS);
				
				ps.setString(1, categorie.getName());
				
				ps.executeUpdate();
				
				connexion.commit();
				
				ResultSet result = ps.getGeneratedKeys();
				
				if(result.next())
					return result.getInt(1);
				
			} catch (SQLException e) 
			{
				e.printStackTrace();			
				try 
				{
					if(connexion != null)
						connexion.rollback();
				} 
				catch (SQLException e1)
				{
					e1.printStackTrace();
				}
					
				throw new DaoException("Impossible de communiquer avec la base de donn�es");
			}
			finally
			{
				try 
				{
					if(connexion != null)
						connexion.close();
				} 
				catch (SQLException e) 
				{
					throw new DaoException("Impossible de communiquer avec la base de donn�es");
				}
			}
			
		return -1;
	}

	@Override
	public boolean doNameIstaken(String name) {
		Connection connexion = null;
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select name from categories where name = ?");
			
			ps.setString(1, name);
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
				return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				if(connexion != null)
					connexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return false;
	}

	@Override
	public int getIdByName(String name) {
		Connection connexion = null;
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("Select id from categories where name = ?");
			
			ps.setString(1, name);
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
				return result.getInt(1);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
				try {
					if(connexion != null)
						connexion.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		return -1;
	}

	@Override
	public Categorie getCategorieById(int id) {
		Connection connexion = null;
		try {
			
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select * from categorie where id = ?");
			ps.setInt(1, id);
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
			{
				Categorie cat = new Categorie(result.getString("name"));
				cat.setId(result.getInt("id"));
				return cat;
			}
					
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
