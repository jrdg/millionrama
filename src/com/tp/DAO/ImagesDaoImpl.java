package com.tp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.tp.beans.Image;
import com.tp.exceptions.DaoException;
import com.tp.interfaceDAO.ImagesDAO;

public class ImagesDaoImpl implements ImagesDAO
{
	private DaoFactory df;

	public ImagesDaoImpl(DaoFactory df)
	{
		this.df = df;
	}
	
	@Override
	public int Add(Image img) throws DaoException {
		Connection connexion = null;
		try {
				
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("insert into images(path,name,produitid) values(?,?,?)" , PreparedStatement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, img.getPath());
			ps.setString(2, img.getFileName());
			ps.setInt(3, img.getProduitId());
			
			ps.executeUpdate();
			connexion.commit();
			
			ResultSet result = ps.getGeneratedKeys();
			
			if(result.next())
				return result.getInt(1);
			
		}
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
		
		return -1;
	}

	@Override
	public ArrayList<Image> getAllByProductId(int productId) {
	
		ArrayList<Image> list = null;
		Connection connexion = null;
		try {
			
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select * from images where produitid = ?");
			
			ps.setInt(1, productId);
			
			ResultSet result = ps.executeQuery();
			
			if(result.isBeforeFirst())
				list = new ArrayList<Image>();
			
			while(result.next())
			{
				Image img = new Image(
					result.getInt("id"),
					result.getInt("produitid"),
					result.getString("name"),
					result.getString("path")
				);
				
				list.add(img);
			}
			
			return list;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			
			try {
				if(connexion != null)
					connexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
	}

	@Override
	public void Delete(int id) throws DaoException {
		Connection connexion = null;
		try {
			
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("delete from images where id = ?");
			
			ps.setInt(1, id);
			
			ps.executeUpdate();
			
			connexion.commit();		
		}
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
	}

	@Override
	public boolean idExist(int id) {
		Connection connexion = null;
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select id from images where id = ?");
			
			ps.setInt(1, id);
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
				return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public ArrayList<Image> getProductImg(int id) {
		Connection connexion = null;
		ArrayList<Image> list = null;
		
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select * from images where produitid = ?");
			
			ps.setInt(1, id);
			
			ResultSet result = ps.executeQuery();
			
			if(result.isBeforeFirst())
				list = new ArrayList<Image>();
			
			while(result.next())
			{
				Image img = new Image(result.getString("name"),result.getString("path"));
				img.setId(result.getInt("id"));
				list.add(img);
			}
			
			return list;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			try {
				if(connexion != null)
					connexion.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
	}

}
