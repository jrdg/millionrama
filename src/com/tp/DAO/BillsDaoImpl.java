package com.tp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.tp.beans.Bill;
import com.tp.beans.Produit;
import com.tp.exceptions.DaoException;
import com.tp.interfaceDAO.ProductsDAO;

public class BillsDaoImpl implements BillsDAO
{
	private DaoFactory df;
	
	public BillsDaoImpl(DaoFactory df)
	{
		this.df = df;
	}

	@Override
	public int Add(Bill bill) throws DaoException {
		Connection connexion = null;
		
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("insert into bill(userid) values(?)",PreparedStatement.RETURN_GENERATED_KEYS);
			
			ps.setInt(1, bill.getUserid());
			
			ps.executeUpdate();
			
			connexion.commit();
			
			ResultSet result = ps.getGeneratedKeys();
			
			if(result.next())
				return result.getInt(1);
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();			
			try 
			{
				if(connexion != null)
					connexion.rollback();
			} 
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
				
			throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}
		finally
		{
			try 
			{
				if(connexion != null)
					connexion.close();
			} 
			catch (SQLException e) 
			{
				throw new DaoException("Impossible de communiquer avec la base de donn�es");
			}
		}
		return -1;
	}

	@Override
	public int AddProductToBill(Produit p , int billId) throws DaoException {
		Connection connexion = null;
		
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("insert into billproduct(billid,productid,qty) values(?,?,?)",PreparedStatement.RETURN_GENERATED_KEYS);
			
			ps.setInt(1, billId);
			ps.setInt(2, p.getId());
			ps.setInt(3, p.getQuantity());
			
			ps.executeUpdate();
			
			connexion.commit();
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();			
			try 
			{
				if(connexion != null)
					connexion.rollback();
			} 
			catch (SQLException e1)
			{
				e1.printStackTrace();
			}
				
			throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}
		finally
		{
			try 
			{
				if(connexion != null)
					connexion.close();
			} 
			catch (SQLException e) 
			{
				throw new DaoException("Impossible de communiquer avec la base de donn�es");
			}
		}
		return -1;
	}

	@Override
	public Bill getBillById(int billId) {
		
		Connection connexion = null;
		ProductsDAO pdao = df.getProductsDao();
		Bill bill = new Bill();
		bill.setId(billId);
		
		try {
			connexion = df.getConnection();
			
			//////////on get le user id du bill/////////////
			PreparedStatement ps = connexion.prepareStatement("select * from bill where id = ?");
			
			ps.setInt(1, billId);

			ResultSet result = ps.executeQuery();
			
			if(result.next())
				bill.setUserid(result.getInt("userid"));
			
			///////////////////on set larraylist de produit lier a se bill//////////
			PreparedStatement ps2 = connexion.prepareStatement("select * from billproduct where billid = ?");
			
			ps2.setInt(1, billId);
			
			ResultSet result2 = ps2.executeQuery();
			
			ArrayList<Produit> plist = new ArrayList<Produit>();
			
			while(result2.next())
			{
				Produit p = pdao.getProduct(result2.getInt("productid"));
				p.setQuantity(result2.getInt("qty"));
				plist.add(p);
			}
			
			bill.setPlist(plist);
			
			return bill;
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();			
		}
		finally
		{
			try 
			{
				if(connexion != null)
					connexion.close();
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
		}
		return null;
	}
}
