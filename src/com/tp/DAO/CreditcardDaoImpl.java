package com.tp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.tp.beans.Creditcard;
import com.tp.exceptions.DaoException;
import com.tp.interfaceDAO.CreditcardsDAO;

public class CreditcardDaoImpl implements CreditcardsDAO
{
	private DaoFactory df;
	
	public CreditcardDaoImpl(DaoFactory df)
	{
		this.df = df;
	}
	
	@Override
	public int Add(Creditcard cc) throws DaoException {
		Connection connexion = null;
		
		try 
		{
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("insert into creditcards(name,cardnumber,userid,type) values(?,?,?,?)",PreparedStatement.RETURN_GENERATED_KEYS);
			
			System.out.println("boom");
			
			ps.setString(1, cc.getName());
			ps.setString(2, cc.getCardnumber());
			ps.setInt(3, cc.getUserid());
			ps.setString(4, cc.getType());
			
			ps.executeUpdate();
			
			connexion.commit();
			
			ResultSet result = ps.getGeneratedKeys();
			
			if(result.next())
				return result.getInt(1);
		}
		catch(SQLException e)
		{		
			e.printStackTrace();
			  try 
			  {
				  if (connexion != null) 
					  connexion.rollback();
	          } 
			  catch (SQLException e2) {
	          }
			  
	          throw new DaoException("Impossible de communiquer avec la base de donn�es");
		}   
		finally 
		{
            try 
            {
                if (connexion != null) 
                    connexion.close();  
            } 
            catch (SQLException e) 
            {
                throw new DaoException("Impossible de communiquer avec la base de donn�es");
            }
        }
		
		
		return -1;
	}

	@Override
	public ArrayList<Creditcard> getAll(int userid) {
		Connection connexion = null;
		ArrayList<Creditcard> list = null;
		
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select * from creditcards where userid = ?");
			
			ps.setInt(1, userid);
			
			ResultSet result = ps.executeQuery();
			
			if(result.isBeforeFirst())
				list = new ArrayList<Creditcard>();
			
			while(result.next())
			{
				Creditcard cc = new Creditcard();
				cc.setType(result.getString("type"));
				cc.setName(result.getString("name"));
				cc.setCardnumber(result.getString("cardnumber"));
				cc.setId(result.getInt("id"));
				cc.setUserid(userid);
				
				list.add(cc);
			}
			
			return list;
		} 
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return null;
	}

	@Override
	public boolean valideCardUser(int userid, int cardid) {
		
		Connection connexion = null;
		
		try {
			connexion = df.getConnection();
			PreparedStatement ps = connexion.prepareStatement("select * from creditcards where id = ? && userid = ?");
			
			ps.setInt(1, cardid);
			ps.setInt(2, userid);
			
			ResultSet result = ps.executeQuery();
			
			if(result.next())
				return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
}
