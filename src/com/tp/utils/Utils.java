package com.tp.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.UUID;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import com.tp.JGclass.JGAttribute;
import com.tp.beans.User;
import com.tp.exceptions.BeanException;

public class Utils 
{	
	public static boolean isDouble(String str)
	{
		try{
			Double.parseDouble(str);
			return true;
		}catch(NumberFormatException e){
			return false;
		}
	}
	
	//hash un string en md5
	public static String hashMD5(String tohash)
	{
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(tohash.getBytes());
			return new String(md.digest());
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	//utiliser pour savoir si la requete est ajax
	public static boolean IsAjax(HttpServletRequest request)
	{
		String ajax = request.getHeader("X-Requested-With");
		
		if(ajax != null)
		{
			if(ajax.equals("XMLHttpRequest"))
				return true;
		}
		
		return false;		
	}
	
	//genere un guid
	public static String generateGuid()
	{
		 UUID uuid = UUID.randomUUID();
	     return uuid.toString();
	}
		
	//se connect a une page et recupere le contenu que celle si renvoi
	public static String readJSON(String url)
	{		
	    String recv;
	    String recvbuff = null;
		URL jsonpage;
		
		try {
			jsonpage = new URL(url);
			URLConnection urlcon = jsonpage.openConnection();
			BufferedReader buffread = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));

			while ((recv = buffread.readLine()) != null)
				recvbuff += recv;
			buffread.close();
			   
			   return recvbuff;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	//test si une chaine de caractere peu etre caster en int
	public static boolean isInt(String str)
	{
		try{
			Integer.parseInt(str);
			return true;
		}catch(NumberFormatException e){
			return false;
		}
	}
}
