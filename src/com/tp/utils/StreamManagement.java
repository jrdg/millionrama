package com.tp.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.http.Part;

public class StreamManagement 
{
	
	public int bufferSize; //taille du buffer
	public String pathToGo; //ou le fichier ira (son chemin)
	public Part part; //le multipart/data
	public String fileName; //le nom du fichier
	
	
	public StreamManagement(int bufferSize, String pathToGo,Part part)
	{
		this.bufferSize = bufferSize;
		this.pathToGo = pathToGo;
		this.part = part;
	}
	
	public void writeFile()
	{
		BufferedInputStream input = null;
		BufferedOutputStream output = null;
		
		//regarde si on a bien un fichier
		this.fileName = this.getFileName(this.part);
		
        // Si on a bien un fichier
        if (this.fileName != null && !this.fileName.isEmpty()) 
        {
            // Corrige un bug du fonctionnement d'Internet Explorer
             this.fileName = this.fileName.substring(this.fileName.lastIndexOf('/') + 1)
                    .substring(this.fileName.lastIndexOf('\\') + 1);
             
     		try
    		{
    			input = new BufferedInputStream(part.getInputStream(),this.bufferSize);
    			output = new BufferedOutputStream(new FileOutputStream(new File(this.pathToGo + this.fileName)), this.bufferSize);
    			
    			byte[] buffer = new byte[this.bufferSize];
    			int longeur;
    			
    			while((longeur = input.read(buffer)) > 0)
    				output.write(buffer, 0 , longeur);
 			
    		} 
     		catch (IOException e) 
     		{
    			e.printStackTrace();
    		}
     		finally
     		{
    			try
    			{
					output.close();
				} 
    			catch (IOException e) 
    			{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    			try 
    			{
					input.close();
				} 
    			catch (IOException e)
    			{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
     		}
        }
        else
        	System.out.println("ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
	}
	
	//rename un fichier un fichier 
	public void renameFile(String fileToChange , File file)
	{
		File f = new File(fileToChange);
		if(f.exists())
			f.renameTo(file);
	}
	
	//recupere le nom du fichier en decoupant le header du multipart
	private String getFileName(Part part)
	{
        for( String contentDisposition : part.getHeader( "content-disposition" ).split( ";" ) ) 
        {
            if( contentDisposition.trim().startsWith( "filename" ) )
            {
                return contentDisposition.substring( contentDisposition.indexOf( '=' ) + 1 ).trim().replace( "\"", "" );
            }
        }
        return null;
	}
	
	public String getJustFileName(Part part)
	{
        for( String contentDisposition : part.getHeader( "content-disposition" ).split( ";" ) ) 
        {
            if( contentDisposition.trim().startsWith( "filename" ) )
            {
                String path = contentDisposition.substring( contentDisposition.indexOf( '=' ) + 1 ).trim().replace( "\"", "" );
                
                String[] splitpart = path.split("\\\\");
                
                return splitpart[splitpart.length-1];
            }
        }      
        return null;
	}

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}

	public String getPathToGo() {
		return pathToGo;
	}

	public void setPathToGo(String pathToGo) {
		this.pathToGo = pathToGo;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}