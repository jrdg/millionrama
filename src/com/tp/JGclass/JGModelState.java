package com.tp.JGclass;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.tp.exceptions.BeanException;
import com.tp.exceptions.JGErrorException;
import com.tp.exceptions.JGValidException;

/*
 * Quand la requete est ajax il faut renvoyer larraylist errors sous forme json dans la page jsp 
 * Quand la requete est normal il faut renvoyer le JGModelState dans la page jsp et utiliser getErrorMsg(nameAttribute) dans la jsp
 */

public class JGModelState 
{
	//Arraylist de JGattribute (un jgattribute contient un name(le nom de lattribut) un msg(msg derreur renovyer) value(valeur envoyer par le formulaire)) 
	private ArrayList<JGAttribute> jgattributeList;
	private ArrayList<String> exemptList;
	
	public JGModelState()
	{
		exemptList = new ArrayList<String>();
	}
	
	//lobjet request de la servlet et className et le nom de classe qui contient les BeanException
	public ArrayList<JGAttribute> getErrors(HttpServletRequest request , String className)
	{		
		jgattributeList = new ArrayList<JGAttribute>();
		
		//on instancie un tableau de class qui contient un la class string
		Class[] paramString = new Class[1];
		paramString[0] = String.class;
		
		//on recupere rous les parametres envoyer par le formulaire 
		Enumeration<String> params = request.getParameterNames();
			
		Class cls = null;
		Object obj = null;
		
		//on cree un instance de la classe qui contient les beanexception
		try{
			 cls = Class.forName(className);
			 obj = cls.newInstance();
		}catch(Exception e)
		{
			
		}
		
		//on parcour le tableau de parametre envoyer par le formulaire
		while(params.hasMoreElements())
		{
			String key = null;	
			String errormsg = null;
			String validmsg = null;
			String value = null;
			
			try
			{
				// key = le nom du parametre
				key = params.nextElement();	
				System.out.println(key);
				//si le parametre qui key nes pas exempter de la validation on traite
				if(!this.isExempted(key))
				{
					//value = la valeur du parametre
					value = request.getParameter(key);
					
					//on va faire une reflection sur la method don on a besoin pour la validation exemple setFirstname
					Method method = cls.getDeclaredMethod("set"+key, paramString);		
					
					//on linvoke
					method.invoke(obj, new String(value));
				}
								
				//si il y a une exception ou un BeanException qui est un erreur de validation formulaire
			}catch(Exception ex)
			{
				//on recupere le nom de lexception pour savoir si ses un Valid ou Erreur exception
				String exceptionClassName = ex.getCause().getClass().getName();
				
				/*
				 * on test si lexception est de type valid ou error et on
				 * met le message dans la variable qui es dedier a cette exception
				 * valid = validmsg error = errormsg
				 */
				if(exceptionClassName.equals(JGValidException.class.getName()))
					validmsg = ex.getCause().getMessage();
				else if(exceptionClassName.equals(JGErrorException.class.getName()))
					errormsg = ex.getCause().getMessage();
			}
			finally
			{
				//et puis finalement on joute au arraylist un JGAttribute qui contient un name un msg et un value
				jgattributeList.add(new JGAttribute(key,errormsg,validmsg,value));
			}
		}
		
		//on retourne le tableau
		return jgattributeList;
	}
	
	/*
	 * on parcour le larraylist pour voir si un JGAttribute a un msg 
	 * si oui on renvoir true car sa veu dire quil y a des erreurs
	 */
	public boolean doHaveError()
	{
		for (JGAttribute f: this.jgattributeList)
		{
			if(f.getMsg() != null)
				return true;
		}
		
		return false;
	}
	
	/*
	 * a utiliser si il faut remove les value envoyer par le formulaire pour raison de securiter
	 * exemple : on fais une requete ajax et quand on renvoi le arraylist dans la page
	 * on ne veu pas que les valeur puisse etre vue car elle peuvent contenir des mot de passe
	 */
	public void removeAllValue()
	{
		for (JGAttribute f: this.jgattributeList)
		{
			if(f.getValue() != null)
				f.setValue(null);
		}
	}
	
	//renvoie la valeur de lattribut passer en parametre qui es dans larraylist sous forme de string
	public String getStringValue(String nameAttribute)
	{
		for (JGAttribute f : jgattributeList) 
		{
			if(f.getName().equals(nameAttribute))
				return f.getValue();
		}
		
		return null;
	}
	
	//renvoie la valeur de lattribut passer en parametre qui es dans larraylist sous forme de int
	public int getIntValue(String nameAttribute)
	{
		for (JGAttribute f : jgattributeList) 
		{
			if(f.getName().equals(nameAttribute))
				return Integer.parseInt(f.getValue());
		}

		return -1;
	}
	
	//renvoie la valeur de lattribut passer en parametre qui es dans larraylist sous forme de double
	public Double getDoubleValue(String nameAttribute)
	{
		for (JGAttribute f : jgattributeList) 
		{
			if(f.getName().equals(nameAttribute))
				return Double.parseDouble(f.getValue());
		}
		
		return -1.0;
	}
	
	//renvoie la valeur de lattribut passer en parametre qui es dans larraylist sous forme de boolean
	public boolean getBoolValue(String nameAttribute)
	{
		for (JGAttribute f : jgattributeList) 
		{
			if(f.getName().equals(nameAttribute))
				return Boolean.parseBoolean(f.getValue());
		}
		
		return false;
	}
	
	//a utiliser si on a dautre erreur a rajoute par exemple dans le cas dun email deja prit
	public void addModelStateError(String nameAttribute , String errormsg)
	{
		for (JGAttribute f : jgattributeList) 
		{
			if(f.getName().equals(nameAttribute))
				f.setMsg(errormsg);
		}
	}
	
	//a utiliser pour ajouter un message de validation global on lutilise generalement sur le parametre le plus haut dans un formulaire
	public void addModelStateValidMsg(String nameAttribute , String validmsg)
	{
		for(JGAttribute f : jgattributeList)
		{
			if(f.getName().equals(nameAttribute))
				f.setValidMsg(validmsg);
		}
	}
	
	//retourne le msg derreur assosier a cette attribut (a utiliser quand ce nes pas une requete ajax)
	public String getErrorMsg(String nameAttribute)
	{
		for (JGAttribute f : jgattributeList) 
		{
			if(f.getName().equals(nameAttribute))
				return f.getMsg();
		}
		
		return null;
	}
	
	//retourne le msg de quand ces valide assosier a cette attribut (a utiliser quand ce nes pas une requete ajax)
	public String getValidMsg(String nameAttribute)
	{
		for (JGAttribute f : jgattributeList) 
		{
			if(f.getName().equals(nameAttribute))
				return f.getValidMsg();
		}
		
		return null;
	}
	
	/*
	 * utiliser pour exempter un attribut du traitement de getErrors
	 * SI IL Y A LIEU IL FAU TOUJOUR EXEMPTER AVANT DUTILISER DINITIALISER UN ARRAYLSIT AVEC GETERRORS
	 */
	public void exempt(String nameAttribute)
	{
		this.exemptList.add(nameAttribute);
	}
	
	//remove lerreur du parametre
	public void removeParameterErr(String nameAttribute)
	{
		for(int i = 0 ; i <= jgattributeList.size()-1 ; i++)
		{
			if(this.jgattributeList.get(i).getName().equals(nameAttribute))
				this.jgattributeList.get(i).setMsg(null);
		}
	}
	
	//retour vrai si la vriable est exempter de la validation
	public boolean isExempted(String nameAttribute)
	{
		for (String exempt : exemptList) 
		{
			if(exempt.equals(nameAttribute))
				return true;
		}
		
		return false;
	}
	
	/*
	 * a utiliser pour reinitialiser lexemtp list par exemple si on a un formulaire setting et
	 */
	public void reinitializeExempteList()
	{
		this.exemptList = new ArrayList<String>();
	}
	
}

