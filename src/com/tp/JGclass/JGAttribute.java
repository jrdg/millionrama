package com.tp.JGclass;

public class JGAttribute
{
	private String name; //Nom du parametre exemple setFirstname = Firstname
	private String msg; //message derreur qui sera la BeanException recupere si il y a lieu
	private String value; //la valeur renvoyer par le formulaire
	private String validMsg; // la valeur du message quand tous ses bien passer
	
	public JGAttribute(String name , String msg)
	{
		this.name = name;
		this.msg = msg;
	}
	
	public JGAttribute(String name , String msg , String value)
	{
		this.name = name;
		this.msg = msg;
		this.value = value;
	}
	
	public JGAttribute(String nameAttribute, String errorMsg , String validMsg , String value)
	{
		this.name = nameAttribute;
		this.msg = errorMsg;
		this.validMsg = validMsg;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValidMsg() {
		return validMsg;
	}

	public void setValidMsg(String validMsg) {
		this.validMsg = validMsg;
	}
	
	
}
