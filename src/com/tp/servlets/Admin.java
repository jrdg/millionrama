package com.tp.servlets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;
import com.tp.DAO.DaoFactory;
import com.tp.JGclass.JGAttribute;
import com.tp.JGclass.JGModelState;
import com.tp.beans.Categorie;
import com.tp.beans.Image;
import com.tp.beans.Panier;
import com.tp.beans.Produit;
import com.tp.beans.User;
import com.tp.exceptions.DaoException;
import com.tp.forms.AddCategorieForm;
import com.tp.forms.AddProductAdminForm;
import com.tp.forms.EditProductForm;
import com.tp.forms.ManageProductForm;
import com.tp.interfaceDAO.CategoriesDAO;
import com.tp.interfaceDAO.ImagesDAO;
import com.tp.interfaceDAO.ProductsDAO;
import com.tp.utils.StreamManagement;
import com.tp.utils.Utils;

/**
 * Servlet implementation class Admin
 */
@WebServlet("/admin")
@MultipartConfig(location="C:/tmp", fileSizeThreshold=1048576000, maxFileSize=2048576000, maxRequestSize=524288000)
public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private JGModelState helper;
	private DaoFactory df;
	private CategoriesDAO catdao;
	private Gson gson;
	private ProductsDAO proddao;
	private ImagesDAO imgdao;
	private static String newImgPath = "C:/tp_millionaire/millionrama_1/WebContent/productimg/";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		helper = new JGModelState();
		df = DaoFactory.getInstance();
		catdao = df.getCategoriesDao();
		proddao = df.getProductsDao();
		imgdao = df.getImageDao();
		gson = new Gson();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(panier != null)
			nbele = panier.getPanier().size();
		
		request.setAttribute("nbele", nbele);
		
		User user = (User) session.getAttribute("user");
		
		if(user != null)
		{
			if(!user.getRole().getName().equals("admin"))
				response.sendRedirect("index");
			else
			{
				ArrayList<Categorie> categories = this.catdao.getAll();
				request.setAttribute("categories", categories);
				this.getServletContext().getRequestDispatcher("/WEB-INF/admin.jsp").forward(request, response);		
			}	
		}
		else
			response.sendRedirect("index");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		
		if(user == null)
			response.sendRedirect("index");
		else
		{
			if(!user.getRole().getName().equals("admin"))
				response.sendRedirect("index");
		}
		
		//si la requete es ajax
		if(Utils.IsAjax(request))
		{
			Part part = null;
			int categorieId = -1;
			//on recupere le type quon envoi exemple si ses addcategorie ou addproduct
			String type = request.getParameter("type");
			
			System.out.println("typeeeeeeeeeeeeeeeee : " + type);
			System.out.println("produit id : "+request.getParameter("productId"));
			
			//on test si le type nest pas null
			if(type != null)
			{
				
				if(helper.isExempted("type"))
					System.out.println("exempteddddddddddddddddddddddddddddddddddd");
				
				/*
				 * on exempte le parametre type de la validation du helper
				 *  et PartErr( ses le parametre qui va servir uniquequement
				 *  derreur au champ ajout dimage dun edit )
				 */
				helper.exempt("type");	
				helper.exempt("PartErr");
				
				/*
				 * on instance un string a null qui va prendre le nom de classe selon le type de requete 
				 * exemple si le type est add categorie ou add produit
				 */
				String classname = null;
				
				//on fais les test de type
				if(type.equals("addcategorie"))
					classname = AddCategorieForm.class.getName();
				else if(type.equals("addproduct"))
					classname = AddProductAdminForm.class.getName();
				else if(type.equals("productmanage"))
					classname = ManageProductForm.class.getName();
				else if(type.equals("editproduct"))		
					classname = EditProductForm.class.getName();
				else if(type.equals("getproductimg"))
				{
					String id = request.getParameter("productId");
					
					if(Utils.isInt(id))
					{
						ArrayList<Image> imglist = this.imgdao.getAllByProductId(Integer.parseInt(id));
						
						String j = gson.toJson(imglist);
						response.getWriter().print(j);
					}
					
					return;
				}
				else if(type.equals("getproductimgSleep"))
				{
					String id = request.getParameter("productId");
					
					try {
						Thread.sleep(6000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(Utils.isInt(id))
					{
						ArrayList<Image> imglist = this.imgdao.getAllByProductId(Integer.parseInt(id));
						
						String j = gson.toJson(imglist);
						response.getWriter().print(j);
					}
					
					return;
				}
				else if(type.equals("deleteproduct"))
				{
					String delete = request.getParameter("delete");
					
					if(delete != null)
					{					
						String[] split = delete.split("-");
						
						for (String str : split) 
						{
							if(Utils.isInt(str))
							{
								try {
									this.proddao.Delete(Integer.parseInt(str));
								} catch (NumberFormatException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (DaoException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
						
						String str = "true";
						String str2 = gson.toJson(split);
						response.getWriter().print(str2);
					}
					else
					{
						String str = "false";
						String str2 = gson.toJson(str);
						response.getWriter().print(str2);
					}
					
					return;
				}
				else if(type.equals("deleteimg"))
				{
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					String imgid = request.getParameter("imgid");
					
					if(Utils.isInt(imgid))
					{
						int idimg = Integer.parseInt(imgid);
						
						if(imgdao.idExist(idimg))
						{
							try {
								imgdao.Delete(idimg);
							} catch (DaoException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							String str = "true";
							String str2 = gson.toJson(str);
							response.getWriter().print(str2);
							return;
						}
					}
					
					String str = "false";
					String str2 = gson.toJson(str);
					response.getWriter().print(str2);
					return;
				}
				
				//on instancie un arraylist de JGAttirbute qui contient le nom les valeur et les msg(erreur) de chaque parametre
				ArrayList<JGAttribute> errors = helper.getErrors(request, classname);		
				
				//on test si la categorie existe deja si ses le cas on ajoute un erreur au model
				
				if(type.equals("addcategorie"))
				{
					if(this.catdao.doNameIstaken(helper.getStringValue("Name")))
						helper.addModelStateError("Name", "Categorie name is already taken" );
				}
				else if(type.equals("addproduct"))
				{
					/*
					 * si le categorie name renvoyer par le formulaire dajout de produit
					 * nest pas dans la base de donner on renvoi un erreur disant que la catergie 
					 * nest pas valide
					 */
					
					categorieId = this.catdao.getIdByName(helper.getStringValue("Categoriename"));
					
					if(categorieId == -1)
						helper.addModelStateError("Categoriename", "The categorie is not valid");
				}
				else if(type.equals("productmanage"))
					categorieId = this.catdao.getIdByName(helper.getStringValue("Categoriename"));
				else if(type.equals("editproduct"))
				{
					part = (Part)request.getPart("productEditPart");
					
					if(part != null)
					{
						if(!(part.getContentType().equals("image/png") || part.getContentType().equals("image/jpeg") || part.getContentType().equals("image/jpg")))
							helper.addModelStateError("PartErr", "Only accept jpg and png");	
					}
				}
				
				
				/*
				 * On test si il y des erreur si ses le cas on remove les value 
				 * car on va rediriger dans la page jsp et on ne veu pas que lutilisateur
				 * puisse recevoir des valeur du genre des mot de passe 
				 * ensuite on parle larraylist errors et on le transmet a la page
				 */
				if(helper.doHaveError())
				{
					helper.removeAllValue();
					String errorsJson = gson.toJson(errors);	
					response.getWriter().print(errorsJson);
				}
				else
				{
					try {
						//si il ny a pas eur derreur et que le type est addcategorie
						if(type.equals("addcategorie"))
							this.catdao.Add(new Categorie(helper.getStringValue("Name")));
						else if(type.equals("addproduct"))
						{
							//si le type est addproduct on ajoute le produit a la bd
							Produit produit = new Produit();
							produit.setName(helper.getStringValue("Name"));
							produit.setDescription(helper.getStringValue("Description"));
							produit.setPrice(helper.getDoubleValue("Price"));
							produit.setCategorieId(categorieId);
							produit.setQuantity(helper.getIntValue("Qty"));							
				
							int productId = this.proddao.Add(produit);
							
							helper.addModelStateValidMsg("Name", helper.getStringValue("Name")+" have been add to the products");
						}
						else if(type.equals("productmanage"))
						{					
							ArrayList<Produit> list = this.proddao.getProductByNameAndOrCategorie(categorieId, helper.getStringValue("Name"));
							
							Gson g = new GsonBuilder().create();
							String pmlist = g.toJson(list);
							response.getWriter().print(pmlist);					
						}
						else if(type.equals("editproduct"))
						{
							Produit p = new Produit();
							p.setId(helper.getIntValue("Id"));
							p.setDescription(helper.getStringValue("Description"));
							p.setName(helper.getStringValue("Name"));
							p.setPrice(helper.getDoubleValue("Price"));
							p.setQuantity(helper.getIntValue("Qty"));
							p.setCategorieId(this.catdao.getIdByName(helper.getStringValue("Categoriename")));
							
							this.proddao.Edit(p);
							
																										
							//////////////////////////////////////////////////////
							//////////LE BLOCK DE CODE QUI AJOUTE LIMAGE//////////
							//////////////////////////////////////////////////////
							if(request.getHeader("Content-type").contains("multipart/form-data"))
							{																		
								if(part != null)
								{								
									StreamManagement sm = new StreamManagement(50240,newImgPath,part); // on instancie le streamanagement
									sm.writeFile(); //on ecrit le fichier dans notre dossier

									try {
										int imgid = this.imgdao.Add(new Image(Integer.parseInt(request.getParameter("Id")) ,sm.getJustFileName(part) ,newImgPath  ));
										String newname = imgid+"_ipid="+sm.getJustFileName(part); //string qui coneitnt le nom que lon va donner au fichier en le renommant
										sm.renameFile(newImgPath+sm.getJustFileName(part), new File(newImgPath+newname)); // on renomme le fichier
									} catch (NumberFormatException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (DaoException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}		
								}	
								else
									System.out.println("nullllllllllllllllllllllllllllllllll");
							}	
							
							//////////////////////////////////////////////////////
							//////////////////////////////////////////////////////
							//////////////////////////////////////////////////////

							helper.addModelStateValidMsg("Name", "Product have been updated");
						}
						
						if(type.equals("addproduct") || type.equals("addcategorie") || type.equals("editproduct"))
						{
							String errorsJson = gson.toJson(errors);
							response.getWriter().print(errorsJson);
						}
						
					} catch (DaoException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}		
			}
		}
		else
			response.sendRedirect("index");
	}

}
