package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tp.DAO.DaoFactory;
import com.tp.DAO.GuidUsersDaoImpl;
import com.tp.beans.Categorie;
import com.tp.beans.GuidUser;
import com.tp.beans.Panier;
import com.tp.exceptions.DaoException;
import com.tp.interfaceDAO.CategoriesDAO;
import com.tp.interfaceDAO.GuidUsersDAO;
import com.tp.interfaceDAO.UsersDAO;
import com.tp.utils.Utils;

/**
 * Servlet implementation class Activation
 */
@WebServlet("/activation")
public class Activation extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private DaoFactory df;   
    private GuidUsersDAO gud;
    private CategoriesDAO catdao;
    private UsersDAO ud;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Activation() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() throws ServletException
    {
        df = DaoFactory.getInstance();
        gud = df.getGuidDao();
        ud = df.getUsersDao();
        catdao = df.getCategoriesDao();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(panier != null)
			nbele = panier.getPanier().size();
		
		request.setAttribute("nbele", nbele);
		
		//on recupere le userid dans lurl
		String userid = request.getParameter("uid");
		
		//on recupere le guid dans lurl
		String guid = request.getParameter("guid");
		
		//on test pour savoir si le userid es bien un int
		if(Utils.isInt(userid))
		{
			//on parse le userid
			int uid = Integer.parseInt(userid);
			
		
			GuidUser guiduser = null;
			
			 // on recupere un objet de type GuidUser qui va contenir le userid le guid et le user
			try {
				guiduser = this.gud.getGuid(uid, guid);
			} catch (DaoException e) {
				e.printStackTrace();
			}
			
			/*
			 * si le GuidUser recuperer plus haut nest pas null on va redireger vers la page activation
			 * avec la variable user qui contiendra le user de lobjet GuidUser
			 * si lobjet es null sa veu dire que ce guid a deja ete activer ou quil es inexistant
			 * alors on redirige vers notfound car cette url nest plus valide
			 */
			if(guiduser != null)
			{
				request.setAttribute("user", guiduser.getUser());
				
				/*
				 * on desactive le guid assosier a linscription de ce user
				 * et on active ce user pour quil puisse se connecter
				 */
				try {
					this.gud.desactive(guiduser.getId());
					this.ud.Activate(guiduser.getUser().getId());
				} catch (DaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				ArrayList<Categorie> categories = this.catdao.getAll();
				request.setAttribute("categories", categories);
				this.getServletContext().getRequestDispatcher("/WEB-INF/activation.jsp").forward(request, response);
			}
			else
				response.sendRedirect("notfound");
				
		}
		else
			response.sendRedirect("notfound");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
