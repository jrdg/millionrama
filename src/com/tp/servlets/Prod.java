package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tp.DAO.DaoFactory;
import com.tp.beans.Categorie;
import com.tp.beans.Image;
import com.tp.beans.Panier;
import com.tp.beans.Produit;
import com.tp.interfaceDAO.CategoriesDAO;
import com.tp.interfaceDAO.ImagesDAO;
import com.tp.interfaceDAO.ProductsDAO;
import com.tp.utils.Utils;

/**
 * Servlet implementation class Produit
 */
@WebServlet("/p")
public class Prod extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private ImagesDAO imgdao;
	private ProductsDAO pd;
	private CategoriesDAO catdao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Prod() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		df = DaoFactory.getInstance();
		imgdao = df.getImageDao();
		pd = df.getProductsDao();
		catdao = df.getCategoriesDao();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(panier != null)
			nbele = panier.getPanier().size();
		
		request.setAttribute("nbele", nbele);
		
		String id = request.getParameter("id");
		
		if(id != null)
		{			
			if(Utils.isInt(id))
			{
				int pid = Integer.parseInt(id);

				Produit produit = pd.getProduct(pid);
				
				if(produit != null)
				{			
					boolean alreadyIn = false;
					
					if(panier != null)
					{
						if(panier.getPanier().get(pid) != null)
							alreadyIn = true;
					}
					
					request.setAttribute("alreadyIn", alreadyIn);
					ArrayList<Image> imglist = imgdao.getProductImg(pid);
					produit.setImages(imglist);
					request.setAttribute("product", produit);
					
					ArrayList<Categorie> categories = this.catdao.getAll();
					request.setAttribute("categories", categories);	
					
					this.getServletContext().getRequestDispatcher("/WEB-INF/p.jsp").forward(request, response);
				}
				else
					response.sendRedirect("notfound");
			}			
		}
		else
		{
			response.sendRedirect("notfound");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
