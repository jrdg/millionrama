package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tp.DAO.DaoFactory;
import com.tp.JGclass.JGModelState;
import com.tp.beans.Categorie;
import com.tp.beans.Panier;
import com.tp.interfaceDAO.CategoriesDAO;
import com.tp.utils.GestionMail;

/**
 * Servlet implementation class Contact
 */
@WebServlet(name = "contact", urlPatterns = { "/contact" })
public class Contact extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private CategoriesDAO catdao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Contact() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		df = DaoFactory.getInstance();
		catdao = df.getCategoriesDao();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(panier != null)
			nbele = panier.getPanier().size();
		
		request.setAttribute("nbele", nbele);

		ArrayList<Categorie> categories = this.catdao.getAll();
		request.setAttribute("categories", categories);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/contact.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String destinataire = request.getParameter("email");
		String nom = request.getParameter("nom");
		String message =request.getParameter("message");
		String destinataire1 = "jf.pgrenier@gmail.com";
		String destinataire2 = "etudiant.isi.java2@gmail.com";

		String msgUsager = "<!DOCTYPE html>" + "<html>" + "<body>" + "<h1>Bonjour " + nom + "</h1>"
				+ "<p>Merci de nous envoyer votre commentaire pertinent. </p>" +
				"<p>Un technicien vous recontactera dans les plus bref delais  </p>" 
				+"<br>"
				+"<br>" 
				+"<br>" 
				+"<p> Cordinalement, l'equipe MillionRama </p>"
				+ "</body>" + "</html>";
		
		
		String administrateur = "<!DOCTYPE html>" + "<html>" + "<body>" + "<h1>Bonjour " + nom + "</h1>"
				+ "<p>Vous avez recu une suggestion </p>" 
				+"<p> Nom: " + nom +"</p>"
				+"<p> courriel: " + destinataire +"</p>"
				+"<p>Voici le message:  </p>" 
				+"<p>"+ message + " </p>" 
				+"<br>" 
				+"<br>" 
				+ "</body>" + "</html>";

		String sujet = "MillionRama - Reception de votre message";
		String sujetAdmin = "MillionRama - Reception d'une demande client";

		GestionMail.sendEmail(msgUsager, destinataire, sujet);
		GestionMail.sendEmail(administrateur, destinataire, sujetAdmin);
		
		response.sendRedirect("index");

	}

}
