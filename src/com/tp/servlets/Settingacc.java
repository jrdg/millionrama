package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tp.DAO.DaoFactory;
import com.tp.JGclass.JGAttribute;
import com.tp.JGclass.JGModelState;
import com.tp.beans.Creditcard;
import com.tp.beans.User;
import com.tp.exceptions.DaoException;
import com.tp.forms.SettingAddCreditCardForm;
import com.tp.interfaceDAO.CreditcardsDAO;
import com.tp.utils.Utils;

/**
 * Servlet implementation class Settingacc
 */
@WebServlet("/settingacc")
@MultipartConfig
public class Settingacc extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private CreditcardsDAO cd;
	private Gson gson;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Settingacc() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		df = DaoFactory.getInstance();
		cd = df.getCreditcardsDao();
		gson = new Gson();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		
		if(user == null)
			response.sendRedirect("index");
		else
		{
			if(Utils.IsAjax(request))
			{
				JGModelState helper = new JGModelState();
				
				ArrayList<JGAttribute> errors = helper.getErrors(request, SettingAddCreditCardForm.class.getName());
				
				if(!helper.doHaveError())
				{
					try {
						this.cd.Add(new Creditcard
						(
								helper.getStringValue("Cardtype"),
								helper.getStringValue("NameOnCard"),
								helper.getStringValue("CardNumber"),
								user.getId()
						));
						helper.addModelStateValidMsg("NameOnCard", "Card have been add");
					} catch (DaoException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				String j = gson.toJson(errors);
				response.getWriter().print(j);
			}
		}			
	}

}
