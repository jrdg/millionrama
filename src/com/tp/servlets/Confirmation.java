package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tp.DAO.DaoFactory;
import com.tp.beans.Categorie;
import com.tp.beans.Creditcard;
import com.tp.beans.Panier;
import com.tp.beans.Produit;
import com.tp.beans.User;
import com.tp.interfaceDAO.CategoriesDAO;
import com.tp.interfaceDAO.CreditcardsDAO;
import com.tp.interfaceDAO.ProductsDAO;

/**
 * Servlet implementation class Confirmation
 */
@WebServlet("/confirmation")
public class Confirmation extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private ProductsDAO pd;
	private CategoriesDAO catdao;
	private CreditcardsDAO cd;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Confirmation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		df = DaoFactory.getInstance();
		pd = df.getProductsDao();
		catdao = df.getCategoriesDao();
		cd = df.getCreditcardsDao();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(user == null)
			response.sendRedirect("cart");
		else
		{
			ArrayList<Creditcard> clist = cd.getAll(user.getId());
			request.setAttribute("clist", clist);
			if(panier == null)
				response.sendRedirect("cart");
			else
			{
				if(panier.getPanier().size()  > 0)
				{
					Iterator listitem = panier.getPanier().entrySet().iterator();
					
					while(listitem.hasNext())
					{
						Map.Entry pair = (Map.Entry)listitem.next();
						
						Produit p = (Produit) pair.getValue();
						if(pd.getProduct(p.getId()) != null)
							panier.updatePrice(p.getId(), pd.getProduct(p.getId()).getPrice());
						else
							listitem.remove();
					}
					
					nbele = panier.getPanier().size();
					
					request.setAttribute("nbele", nbele);
					
					if(nbele > 0)
						request.setAttribute("globalprice", panier.calculateGlobalPrice());
					
					ArrayList<Categorie> categories = this.catdao.getAll();
					request.setAttribute("categories", categories);	
					this.getServletContext().getRequestDispatcher("/WEB-INF/confirmation.jsp").forward(request, response);
				}
				else
					response.sendRedirect("cart");
			}
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

}
