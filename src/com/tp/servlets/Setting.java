package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tp.DAO.DaoFactory;
import com.tp.JGclass.JGAttribute;
import com.tp.JGclass.JGModelState;
import com.tp.beans.Categorie;
import com.tp.beans.Creditcard;
import com.tp.beans.Panier;
import com.tp.beans.User;
import com.tp.exceptions.DaoException;
import com.tp.forms.SettingChangeUserForm;
import com.tp.interfaceDAO.CategoriesDAO;
import com.tp.interfaceDAO.CreditcardsDAO;
import com.tp.interfaceDAO.UsersDAO;
import com.tp.utils.Utils;

/**
 * Servlet implementation class Setting
 */
@WebServlet("/setting")
@MultipartConfig
public class Setting extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private UsersDAO ud;
	private CreditcardsDAO cc;
	private CategoriesDAO catdao;
	private Gson gson;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Setting() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() throws ServletException
    {
        gson = new Gson();
        df = DaoFactory.getInstance();
        cc = df.getCreditcardsDao();
        ud = df.getUsersDao();
        catdao = df.getCategoriesDao();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		HttpSession session = request.getSession();
		
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(panier != null)
			nbele = panier.getPanier().size();
		
		request.setAttribute("nbele", nbele);
		
		User user = (User) session.getAttribute("user");
		
		if(user == null)
			response.sendRedirect("index");
		else
		{
			ArrayList<Creditcard> list = this.cc.getAll(user.getId());
			
			if(list != null)
				request.setAttribute("cclist", list);
			
			ArrayList<Categorie> categories = this.catdao.getAll();
			request.setAttribute("categories", categories);
			this.getServletContext().getRequestDispatcher("/WEB-INF/setting.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		
		if(user == null)
			response.sendRedirect("index");
		else
		{	
			if(Utils.IsAjax(request))
			{
				JGModelState helper = new JGModelState();
				
				//on va chercher tous les erreur et message de vlaidation
				ArrayList<JGAttribute> errors = helper.getErrors(request, SettingChangeUserForm.class.getName());
					
				//on test voir si le user a ecrit quelque chose dans le sinput pw et pwc
				if(helper.getStringValue("Password").isEmpty() && helper.getStringValue("PasswordConfirmation").isEmpty())
				{
					/*
					 * si se sle cas on remove les err de ces 3 champ 
					 * car on veu que le user puisse editer tout le formulaire en appuyant
					 * seuleemtn sur un seul bouton
					 */
					helper.removeParameterErr("Password");
					helper.removeParameterErr("PasswordConfirmation");
					helper.removeParameterErr("Oldpassword");
				}
				else
				{
					if(!Utils.hashMD5(helper.getStringValue("Oldpassword")).equals(user.getPassword()))
						helper.addModelStateError("Oldpassword", "Old password must be equal to your current password");
				}
		
				
				
				if(!helper.getStringValue("Email").equals(user.getEmail()))
				{
					if(ud.doEmailIsTaken(helper.getStringValue("Email")))
						helper.addModelStateError("Email", "Email is already taken");
				}
				
				
				//si il ny a pas derreur on ajoute un message de validation 
				if(!helper.doHaveError())
				{
					helper.addModelStateValidMsg("Oldpassword", "Profil have been updated");
					
					User u = new User();
					
					u.setEmail(helper.getStringValue("Email"));
					u.setPassword(helper.getStringValue("Password"));
					u.setId(user.getId());
					
					try {
						ud.Update(u);
					} catch (DaoException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					user.setEmail(helper.getStringValue("Email"));
					
					if(!(helper.getStringValue("Password").isEmpty() || helper.getStringValue("Password") == null))
						user.setPassword(Utils.hashMD5(helper.getStringValue("Password")));
					
					session.setAttribute("user", user);
				}
				
				helper.removeAllValue();
				String j = gson.toJson(errors);		
				response.getWriter().print(j);
			}
		}		
	}

}
