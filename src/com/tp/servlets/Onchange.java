package com.tp.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tp.DAO.DaoFactory;
import com.tp.beans.Panier;
import com.tp.beans.Produit;
import com.tp.interfaceDAO.ProductsDAO;
import com.tp.utils.Utils;

/**
 * Servlet implementation class Onchange
 */
@WebServlet("/onchange")
@MultipartConfig
public class Onchange extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private ProductsDAO pd;
	private Gson gson;
       
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Onchange() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		df = DaoFactory.getInstance();
		pd = df.getProductsDao();
		gson = new Gson();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(Utils.IsAjax(request))
		{
			String id = request.getParameter("id");
			String value = request.getParameter("value");
			
			if(Utils.isInt(id) && Utils.isInt(value))
			{
				int pid = Integer.parseInt(id);
				int val = Integer.parseInt(value);
				
				Produit p = pd.getProduct(pid);
				
				if(p != null)
				{
					
					HttpSession session = request.getSession();
					
					Panier panier = (Panier) session.getAttribute("panier");
					
					if(panier != null)
					{
						if(panier.getPanier().get(pid) != null)
						{
							int qty = pd.getProduct(pid).getQuantity();

								String operation = null;
								
								if(val > ((Produit)panier.getPanier().get(pid)).getQuantity())
									operation = "addition";
								else if(val < 	((Produit)panier.getPanier().get(pid)).getQuantity())
									operation = "soustraction";
								else
									operation = "notchange";
								
								if(val > 0 && val <= qty)
									((Produit)panier.getPanier().get(pid)).setQuantity(val);
								else
								{
									((Produit)panier.getPanier().get(pid)).setQuantity(1);
									operation = "notchange";
								}
								
								String[] ar = {"true" , operation};
								
								String j = gson.toJson(ar);
								
								response.getWriter().print(j);
						}
						else
						{
							String[] ar = {"true" , "notchange"};
							
							String j = gson.toJson(ar);
							
							response.getWriter().print(j);
						}
					}
					else
					{
						String[] ar = {"true" , "notchange"};
						
						String j = gson.toJson(ar);
						
						response.getWriter().print(j);
					}
					
					Panier.iterateToString(panier.getPanier());
					
					return;
				}
			}
			
			String[] ar = {"true" , "notchange"};
			
			String j = gson.toJson(ar);
			
			response.getWriter().print(j);;
		}
		else
			response.sendRedirect("notfound");
	}

}
