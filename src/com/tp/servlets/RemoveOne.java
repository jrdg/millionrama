package com.tp.servlets;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tp.DAO.DaoFactory;
import com.tp.beans.Panier;
import com.tp.beans.Produit;
import com.tp.interfaceDAO.ProductsDAO;
import com.tp.utils.Utils;

/**
 * Servlet implementation class RemoveOne
 */
@WebServlet("/removeone")
@MultipartConfig
public class RemoveOne extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private ProductsDAO pd;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveOne() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		df = DaoFactory.getInstance();
		pd = df.getProductsDao();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(Utils.IsAjax(request))
		{
			String id = request.getParameter("id");
			System.out.println("iddddddddd to remove : "+id);
			if(Utils.isInt(id))
			{
				int pid = Integer.parseInt(id);
				
				Produit p = pd.getProduct(pid);
				
				if(p != null)
				{
					
					HttpSession session = request.getSession();
					
					Panier panier = (Panier) session.getAttribute("panier");
					
					if(panier != null)
					{
						if(panier.getPanier().get(pid) != null)
						{
							if(((Produit)panier.getPanier().get(pid)).getQuantity() > 1)
							{
								((Produit)panier.getPanier().get(pid)).setQuantity(((Produit)panier.getPanier().get(pid)).getQuantity()-1);
								 response.getWriter().print("true");
							}
							else
								response.getWriter().print("false");
						}
						else
							response.getWriter().print("false");
					}
					else
						response.getWriter().print("false");
					
					Panier.iterateToString(panier.getPanier());
					
					return;
				}
			}
			
			response.getWriter().print("false");
		}
		else
			response.sendRedirect("notfound");
	}

}
