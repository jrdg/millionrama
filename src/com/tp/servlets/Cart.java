package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tp.DAO.DaoFactory;
import com.tp.beans.Categorie;
import com.tp.beans.Panier;
import com.tp.beans.Produit;
import com.tp.beans.User;
import com.tp.interfaceDAO.CategoriesDAO;
import com.tp.interfaceDAO.ProductsDAO;

/**
 * Servlet implementation class Cart
 */
@WebServlet("/cart")
public class Cart extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private ProductsDAO pd;
	private CategoriesDAO catdao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		df = DaoFactory.getInstance();
		pd = df.getProductsDao();
		catdao = df.getCategoriesDao();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(panier != null)
		{		
			Iterator listitem = panier.getPanier().entrySet().iterator();
			
			while(listitem.hasNext())
			{
				Map.Entry pair = (Map.Entry)listitem.next();
				
				Produit p = (Produit) pair.getValue();
				if(pd.getProduct(p.getId()) != null)
					panier.updatePrice(p.getId(), pd.getProduct(p.getId()).getPrice());
				else
					listitem.remove();
			}
			
			nbele = panier.getPanier().size();
		}
		
		request.setAttribute("nbele", nbele);
		
		if(nbele > 0)
			request.setAttribute("globalprice", panier.calculateGlobalPrice());
		
		ArrayList<Categorie> categories = this.catdao.getAll();
		request.setAttribute("categories", categories);	
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/cart.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		HttpSession session = request.getSession();
		
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(panier != null)
		{
			Iterator listitem = panier.getPanier().entrySet().iterator();
			
			while(listitem.hasNext())
			{
				Map.Entry pair = (Map.Entry)listitem.next();
				
				Produit p = (Produit) pair.getValue();
				if(pd.getProduct(p.getId()) != null)
					panier.updatePrice(p.getId(), pd.getProduct(p.getId()).getPrice());
				else
				{
					listitem.remove();
					
					nbele = panier.getPanier().size();
					request.setAttribute("nbele", nbele);
					
					if(nbele > 0)
						request.setAttribute("globalprice", panier.calculateGlobalPrice());
					
					ArrayList<Categorie> categories = this.catdao.getAll();
					request.setAttribute("categories", categories);	
					
					request.setAttribute("error", p.getName()+" have been remove because the item do not exist anymore please try again");
					this.getServletContext().getRequestDispatcher("/WEB-INF/cart.jsp").forward(request, response);
					return;
				}
			}
			
			User user = (User) session.getAttribute("user");
			
			if(user == null)
			{
				nbele = panier.getPanier().size();
				request.setAttribute("nbele", nbele);
				
				if(nbele > 0)
					request.setAttribute("globalprice", panier.calculateGlobalPrice());
				
				ArrayList<Categorie> categories = this.catdao.getAll();
				request.setAttribute("categories", categories);	
				
				request.setAttribute("error", "You must be login to confirm...click here to login");
				this.getServletContext().getRequestDispatcher("/WEB-INF/cart.jsp").forward(request, response);
				return;
			}
			else
			{
				response.sendRedirect("confirmation");
			}
		}
	}

}
