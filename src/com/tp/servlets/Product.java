package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tp.DAO.DaoFactory;
import com.tp.beans.Categorie;
import com.tp.beans.Panier;
import com.tp.beans.Produit;
import com.tp.interfaceDAO.CategoriesDAO;
import com.tp.interfaceDAO.ImagesDAO;
import com.tp.interfaceDAO.ProductsDAO;
import com.tp.utils.Utils;

/**
 * Servlet implementation class Product
 */
@WebServlet("/product")
public class Product extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private CategoriesDAO catdao;
	private ProductsDAO pd;
	private ImagesDAO imgd;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Product() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		df = DaoFactory.getInstance();
		catdao = df.getCategoriesDao();
		pd = df.getProductsDao();
		imgd = df.getImageDao();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(panier != null)
			nbele = panier.getPanier().size();
		
		request.setAttribute("nbele", nbele);
		
		String searchid = request.getParameter("searchid");
		String searchname = request.getParameter("searchname");
		
		if(Utils.isInt(searchid))
		{
			ArrayList<Produit> plist =  pd.getProductByNameAndOrCategorie2(Integer.parseInt(searchid), searchname);
			
			for (Produit produit : plist)
				produit.setImages(imgd.getProductImg(produit.getId()));
			
			request.setAttribute("plist", plist);
			
			ArrayList<Categorie> categories = this.catdao.getAll();
			request.setAttribute("categories", categories);	
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/product.jsp").forward(request, response);
		}
		else
			response.sendRedirect("index");
		
	}

}
