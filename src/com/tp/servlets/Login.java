package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tp.DAO.DaoFactory;
import com.tp.JGclass.JGAttribute;
import com.tp.JGclass.JGModelState;
import com.tp.beans.Panier;
import com.tp.beans.User;
import com.tp.forms.LoginForm;
import com.tp.interfaceDAO.UsersDAO;
import com.tp.utils.Utils;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df; // on declare le daofactory
	private UsersDAO ud; //on declare linterface userdao
	private JGModelState helper; //on declare un jgmodelstate
	private Gson gson;
	private ArrayList<JGAttribute> errors;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() throws ServletException
    {
        df = DaoFactory.getInstance(); //on instance le daofactory
        ud = df.getUsersDao(); //on instance linterface userdao
        helper = new JGModelState(); //on instancie le jgmodelstate
        gson = new Gson();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(panier != null)
			nbele = panier.getPanier().size();
		
		request.setAttribute("nbele", nbele);
		
		//on instancie un user avec la variable de session user
		User user = (User) session.getAttribute("user"); 
		
		/*
		 * si le user est connecte on redirige a lindex
		 * sinon on affiche la page de login
		 */
		if(user != null)
			response.sendRedirect("index");
		else	
			this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referrer = request.getHeader("referer");
		String[] split = referrer.split("/");
		String servlet = null;
		
		if(split.length-1 == 4)
			servlet = split[4];
		else
			servlet = "index";
		
		System.out.println("refererrrrrrrrrrrrrr : "+referrer);
				
		//instance un array list de JGAttribute a partir du arraylist du JGModelState
		errors = helper.getErrors(request, LoginForm.class.getName());
			
	    //ici si le matchemailpassword est bon sa renvoi le userid sinon sa renvoi -1
		int userId = ud.MatchEmailPassword(helper.getStringValue("Email"), helper.getStringValue("Password"));
		
		/*
		 * on test si le userid == -1 si ses le cas on ajoute lerreur bad identifier or password
		 * car sa veu dire que les information son pas bonne et que aucun user na se email avec ce password
		 * sinon si le user est bon on regarde si il es actif si il ne les pas on lui dit daller activer son compte
		 * par le email
		 */
		if(userId == -1)
			helper.addModelStateError("Email", "Bad identifier or password");
		else
		{
			if(!ud.isActif(userId))
				helper.addModelStateError("Email", "Visit your email and click on the link for activate your account");
		}
		
		//si la requete est ajax
		if(Utils.IsAjax(request))
		{
			/*
			 * on regarde si il y a un erreur de validation dans le arraylist de jgattribute 
			 * ou dans les addmodelstate
			 */
			if(helper.doHaveError())
			{
				/*
				 * si il y a un erreur on remove les value envoyer par le formulaire car on va renvoyer
				 * larraylist dans la page et on ne veu pas que lutilisateur puisse voir dans le javascript
				 * les valeur retourner exemple des passwords
				 */
				helper.removeAllValue();
				String errorsJson = gson.toJson(errors);		
				response.getWriter().print(errorsJson);	
			}
			else
			{
				//si tous es beau on set une variable de session user que lon instancier avec son email
				HttpSession session = request.getSession();
				response.getWriter().print(gson.toJson(servlet));
				session.setAttribute("user", this.ud.getUserByEmail(helper.getStringValue("Email")));
			}
		}
		else
		{		
			//si il y a des erreur on envoi le help qui contient larraylist derreur dans la page jsp
			if(helper.doHaveError())
				request.setAttribute("helper", helper);	
			else
			{
				//si tous es beau on set une variable de session user que lon instancier avec son email
				HttpSession session = request.getSession();
				session.setAttribute("user", this.ud.getUserByEmail(helper.getStringValue("Email")));
			}
			
			doGet(request, response);
		}
	}

}
