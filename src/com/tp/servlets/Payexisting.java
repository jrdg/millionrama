package com.tp.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tp.DAO.BillsDAO;
import com.tp.DAO.DaoFactory;
import com.tp.JGclass.JGAttribute;
import com.tp.JGclass.JGModelState;
import com.tp.beans.Bill;
import com.tp.beans.GuidUser;
import com.tp.beans.Panier;
import com.tp.beans.Produit;
import com.tp.beans.User;
import com.tp.exceptions.DaoException;
import com.tp.exceptions.JGErrorException;
import com.tp.forms.PayWithExisting;
import com.tp.interfaceDAO.CreditcardsDAO;
import com.tp.interfaceDAO.ProductsDAO;
import com.tp.utils.GestionMail;
import com.tp.utils.Utils;

/**
 * Servlet implementation class Payexisting
 */
@WebServlet("/payexisting")
@MultipartConfig
public class Payexisting extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private CreditcardsDAO cd;
	private Gson gson;
	private BillsDAO bd;
	private ProductsDAO pd;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Payexisting() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		df = DaoFactory.getInstance();
		cd = df.getCreditcardsDao();
		gson = new Gson();
		bd = df.getBillsDao();
		pd = df.getProductsDao();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		User user = (User) session.getAttribute("user");
		Panier panier = (Panier) session.getAttribute("panier");
		
		if(user ==  null)
			response.sendRedirect("index");
		
		if(Utils.IsAjax(request))
		{
			JGModelState helper = new JGModelState();
			ArrayList<JGAttribute> errors = helper.getErrors(request, PayWithExisting.class.getName());
			
			String month = helper.getStringValue("Month");
			String year = helper.getStringValue("Year");
			
			//si la carte nappartien pas a ce membre
			if(!cd.valideCardUser(user.getId(), Integer.parseInt(helper.getStringValue("CreditCardId"))))
				helper.addModelStateError("CreditCardId", "Card is not valid");
			
			if(month.isEmpty() || month == null)
				helper.addModelStateError("Month","Month cant be null");
			else if(!Utils.isInt(month))
				helper.addModelStateError("Month","Month need to be a number");
			else if(Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12)
				helper.addModelStateError("Month", "Month need to be between 1 and 12");
			else if(year.isEmpty() || year == null)
				helper.addModelStateError("Month","Year cant be null");
			else if(!Utils.isInt(year))
				helper.addModelStateError("Month","Year need to be a number");
			else if(year.length() != 4)
				helper.addModelStateError("Month","Year need to have 4 character");
		
			
			 SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			 
			 String date = "1/"+helper.getStringValue("Month")+"/"+helper.getStringValue("Year");
			 try {
				Date d = formatter.parse(date);
				
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				Date dnow = new Date();
				
				if(!(dnow.compareTo(d) < 0))
					helper.addModelStateError("Month", "Your card is expired");
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 
			if(!helper.doHaveError())
			{
				Bill bill = new Bill();
				bill.setUserid(user.getId());
				
				try {
					int idBill = bd.Add(bill);
					
					Iterator listitem = panier.getPanier().entrySet().iterator();
					
					while(listitem.hasNext())
					{
						Map.Entry pair = (Map.Entry)listitem.next();
						
						Produit p = (Produit) pair.getValue();
						if(pd.getProduct(p.getId()) != null)
							bd.AddProductToBill(p, idBill);
						else
							listitem.remove();
					}
					
					//on recupere le bill avec tous ses produit pour envoyer par mail
					Bill b = bd.getBillById(idBill);
					b.calculeTotal();
					
					/////////////////////////
					///SEND EMAIL HERE///////
					/////////////////////////
					
					
					String destinataire = user.getEmail();
					String destinataire1 = "jf.pgrenier@gmail.com";
					String destinataire2 = "etudiant.isi.java2@gmail.com";

					String msgUsager = "<!DOCTYPE html>" + "<html>" + "<body>" + "<h1>Bonjour </h1>";
					
					for (Produit p : b.getPlist()) {
						msgUsager += "<p>Nom : "+ p.getName()+" Quantity : "+p.getQuantity()+" Overall price : "+p.getPrice() * p.getQuantity()+"$</p>";
					}
					
					msgUsager += "<p> Total : "+b.getTotal()+"$</p>";
					
					String sujet = "MillionRama - Reception de votre message";
					String sujetAdmin = "MillionRama - Reception d'une demande client";

					GestionMail.sendEmail(msgUsager, destinataire, sujet);
					
					/////////////////////////
					////////////////////////
					////////////////////////
					
					panier.getPanier().clear();
					
				} catch (DaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			response.getWriter().print(gson.toJson(errors));
		}
		
	}

}
