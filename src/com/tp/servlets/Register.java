package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tp.DAO.DaoFactory;
import com.tp.JGclass.JGAttribute;
import com.tp.JGclass.JGModelState;
import com.tp.beans.Categorie;
import com.tp.beans.GoogleAnswer;
import com.tp.beans.GuidUser;
import com.tp.beans.Panier;
import com.tp.beans.Produit;
import com.tp.beans.Role;
import com.tp.beans.User;
import com.tp.exceptions.BeanException;
import com.tp.exceptions.DaoException;
import com.tp.forms.RegisterForm;
import com.tp.interfaceDAO.CategoriesDAO;
import com.tp.interfaceDAO.GuidUsersDAO;
import com.tp.interfaceDAO.ImagesDAO;
import com.tp.interfaceDAO.ProductsDAO;
import com.tp.interfaceDAO.UsersDAO;
import com.tp.managers.UsersManager;
import com.tp.utils.GestionMail;
import com.tp.utils.Utils;

/**
 * Servlet implementation class Register
 */
@WebServlet(name = "register" , urlPatterns = "/index")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String GOOGLE_API_KEY = "6LeXYQkUAAAAAMmFXVay-ko--SI7UFp69Tl7uOzd";
	private UsersDAO userDao;
	private GuidUsersDAO guidDAO;
	private CategoriesDAO catdao;
	private DaoFactory df;
	private Gson gson;
	private ProductsDAO pd;
	private ImagesDAO imgd;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register()
    {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() throws ServletException
    {
        DaoFactory df = DaoFactory.getInstance();
        this.userDao = df.getUsersDao();
        this.guidDAO = df.getGuidDao();
        this.catdao = df.getCategoriesDao();
        gson = new Gson();
        pd = df.getProductsDao();
        imgd = df.getImageDao();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{		
		HttpSession session = request.getSession();
		
		Panier panier = (Panier) session.getAttribute("panier");
		
		int nbele = 0;
		
		if(panier != null)
			nbele = panier.getPanier().size();
		
		request.setAttribute("nbele", nbele);
		
		ArrayList<Produit> plist =  pd.getLast5();
		
		for (Produit produit : plist)
			produit.setImages(imgd.getProductImg(produit.getId()));
		
		request.setAttribute("plist", plist);
		
		ArrayList<Categorie> categories = this.catdao.getAll();
		request.setAttribute("categories", categories);
		this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{		
		if(Utils.IsAjax(request))
		{
			/////////////////////////////////
			//////////POUR LE CAPCHA/////////
			/////////////////////////////////
			
			//on recupere le capcha
			String capcha = request.getParameter("G_recaptcha_response");	
			
			//on recupere le ip
			String ipAddress = request.getRemoteAddr();
			
			//on recupere le json en envoyer une requete a lurl de google
			String googleanswer =  Utils.readJSON("https://www.google.com/recaptcha/api/siteverify?secret="+GOOGLE_API_KEY+"&response="+capcha+"&remoteip="+ipAddress);
			googleanswer = googleanswer.substring(4);
			
			//on transform le json recu par google en objet
			GoogleAnswer g = gson.fromJson(googleanswer, GoogleAnswer.class);
			
			/////////////////////////////////
			////////FIN POUR LE CAPCHA///////
			/////////////////////////////////
						
			//on instancie le JGModelstate
			JGModelState helper = new JGModelState();
			
			//on recupere tous les error du formulaire de register
			ArrayList<JGAttribute> errors = helper.getErrors(request, RegisterForm.class.getName());
			
			//si le capcha a failed
			if(!g.getSucces())
				helper.addModelStateError("G_recaptcha_response", "Invalid capcha");
				
			//si le email es deja prit on rajoute un erreur a lattribut Email qui na pas ete valider dans la bean RegisterForm
			if(this.userDao.doEmailIsTaken(helper.getStringValue("Email")))
				helper.addModelStateError("Email", "Email is already taken");
			
			/*
			 * on regarde si il y a un erreur de validation dans le arraylist de jgattribute 
			 * ou dans les addmodelstate
			 */
			if(helper.doHaveError())
			{
				/*
				 * si il y a un erreur on remove les value envoyer par le formulaire car on va renvoyer
				 * larraylist dans la page et on ne veu pas que lutilisateur puisse voir dans le javascript
				 * les valeur retourner exemple des passwords
				 */
				helper.removeAllValue();
				String errorsJson = gson.toJson(errors);	
				System.out.println(errorsJson);
				response.getWriter().print(errorsJson);	
			}
			else
			{	
				/*
				 * si tous ses bien passer
				 * on ajoute lutilisateur dans la base de donne
				 * et on ajoute le guid avec le id de ce user
				 */
				try {
					int userid = this.userDao.Add(UsersManager.prepareToInsert(helper));
					int guidid = this.guidDAO.add(new GuidUser(userid , Utils.generateGuid()));
					
					/////////////////////////////////////////////
					///////ENVOI DU EMAIL AVEC LE GUID ICI///////
					/////////////////////////////////////////////
					
					GuidUser guid = this.guidDAO.getguid(guidid);
					
					String destinataire = helper.getStringValue("Email");
					String destinataire1 = "jf.pgrenier@gmail.com";
					String destinataire2 = "etudiant.isi.java2@gmail.com";

					String msgUsager = "<!DOCTYPE html>" + "<html>" + "<body>" + "<h1>Bonjour </h1>"
							+ "<p>Welcome to Millionrama please click on the link to activate your account!</p>" +
							"<a href='http://localhost:8080/millionrama/activation?uid="+guid.getUserId()+"&guid="+guid.getGuid()+"+'>click here</a>" 
							+ "</body>" + "</html>";
					
					
					String sujet = "MillionRama - Reception de votre message";
					String sujetAdmin = "MillionRama - Reception d'une demande client";

					GestionMail.sendEmail(msgUsager, destinataire, sujet);
					
					
					
					
					
					
					/////////////////////////////////////////////
					////////////////////////////////////////////
					//////////////////////////////////////////////
					
					
				} catch (DaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				helper.addModelStateValidMsg("Firstname", "Your account have been created please go visit your email we send you a email for the validation");
				response.getWriter().print(gson.toJson(errors));
			}
		}
		else
			doGet(request,response);
	}
}
