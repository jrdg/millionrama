package com.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.tp.DAO.DaoFactory;
import com.tp.JGclass.JGModelState;
import com.tp.beans.Image;
import com.tp.beans.Panier;
import com.tp.beans.Produit;
import com.tp.interfaceDAO.ImagesDAO;
import com.tp.interfaceDAO.ProductsDAO;
import com.tp.utils.Utils;

/**
 * Servlet implementation class test
 */
@WebServlet("/test")
@MultipartConfig
public class test extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DaoFactory df;
	private ProductsDAO pd;
	private ImagesDAO imgd;
	private Gson gson;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public test() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		df = DaoFactory.getInstance();
		pd = df.getProductsDao();
		imgd = df.getImageDao();
		gson = new Gson();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(Utils.IsAjax(request))
		{
			String id = request.getParameter("id");
			
			if(Utils.isInt(id))
			{
				int pid = Integer.parseInt(id);
				
				Produit p = pd.getProduct(pid);
				
				if(p != null)
				{
					
					HttpSession session = request.getSession();
					
					Panier panier = (Panier) session.getAttribute("panier");
					
					if(panier == null)
					{
						panier = new Panier();
						session.setAttribute("panier", panier);
					}
			
					if(p.getQuantity() > 0)
					{
						
						if(panier.getPanier().containsKey(pid))
						{
							
							if(((Produit)panier.getPanier().get(pid)).getQuantity() < p.getQuantity())
							{
								((Produit)panier.getPanier().get(pid)).setQuantity(((Produit)panier.getPanier().get(pid)).getQuantity()+1);
								response.getWriter().print("true");
							}
							else
								response.getWriter().print("false");
							return;
							
						}
						
						
						ArrayList<Image> imglist = imgd.getAllByProductId(pid);
						p.setImages(imglist);
						p.setQuantity(1);
						panier.Add(p);
						Panier.iterateToString(panier.getPanier());
						response.getWriter().print("true");
					}
					else
						response.getWriter().print("false");
					
					
					return;
				}								
			}	
			
			response.getWriter().print("false");
		}
		else
			response.sendRedirect("notfound");
	}

}
