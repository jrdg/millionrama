package com.tp.managers;
import java.util.ArrayList;

import com.tp.JGclass.JGModelState;
import com.tp.beans.User;
import com.tp.exceptions.BeanException;

public class UsersManager 
{
	public static User prepareToInsert(JGModelState model)
	{	
		User user = new User(
				model.getStringValue("Firstname") ,
			    model.getStringValue("Lastname"),
				model.getStringValue("Email"),
				model.getStringValue("Password")
				);	
		
		user.setRoleId(5);
		user.setStatus(false);
		
		return user;
	}
}
