package com.tp.exceptions;

public class JGValidException extends Exception
{
	public JGValidException(String msg)
	{
		super(msg);
	}
}
