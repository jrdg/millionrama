package com.tp.exceptions;

public class JGErrorException extends Exception
{
	public JGErrorException(String msg)
	{
		super(msg);
	}
}
